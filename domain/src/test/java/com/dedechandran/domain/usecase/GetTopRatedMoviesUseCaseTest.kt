package com.dedechandran.domain.usecase

import com.dedechandran.domain.model.TopRatedMovie
import com.dedechandran.domain.repository.MovieRepository
import com.dedechandran.domain.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class GetTopRatedMoviesUseCaseTest {
    @MockK
    lateinit var movieRepository: MovieRepository

    private val scheduler = TrampolineSchedulerProvider()

    private lateinit var getTopRatedMoviesUseCase: GetTopRatedMoviesUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getTopRatedMoviesUseCase = GetTopRatedMoviesUseCase(
            movieRepository = movieRepository,
            baseSchedulerProvider = scheduler
        )
    }

    @Test
    fun buildUseCaseObservable_inUseCase_shouldProduce_observableOfTopRatedMovies() {
        val actualResult = TopRatedMovie(
            page = 1,
            isLastPage = true,
            items = emptyList()
        )
        every { movieRepository.getTopRatedMovies(page = 1) } returns Observable.just(actualResult)

        getTopRatedMoviesUseCase.buildUseCaseObservable(params = GetTopRatedMoviesUseCase.Params(1))
            .test()
            .assertResult(actualResult)
            .assertComplete()
            .dispose()
        verify { movieRepository.getTopRatedMovies(page = 1) }
    }
}
