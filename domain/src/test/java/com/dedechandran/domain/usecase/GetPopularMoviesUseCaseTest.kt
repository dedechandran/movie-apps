package com.dedechandran.domain.usecase

import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.repository.MovieRepository
import com.dedechandran.domain.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class GetPopularMoviesUseCaseTest {
    @MockK
    lateinit var movieRepository: MovieRepository

    private val scheduler = TrampolineSchedulerProvider()

    private lateinit var getPopularMoviesUseCase: GetPopularMoviesUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getPopularMoviesUseCase = GetPopularMoviesUseCase(
            movieRepository = movieRepository,
            baseSchedulerProvider = scheduler
        )
    }

    @Test
    fun buildUseCaseObservable_inUseCase_shouldProduce_observableOfPopularMovies() {
        val actualResult = PopularMovie(
            page = 1,
            isLastPage = true,
            items = emptyList()
        )
        every { movieRepository.getPopularMovies(page = 1) } returns Observable.just(actualResult)

        getPopularMoviesUseCase.buildUseCaseObservable(params = GetPopularMoviesUseCase.Params(1))
            .test()
            .assertResult(actualResult)
            .assertComplete()
            .dispose()
        verify { movieRepository.getPopularMovies(page = 1) }
    }
}
