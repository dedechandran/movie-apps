package com.dedechandran.domain.usecase

import com.dedechandran.domain.repository.MovieRepository
import com.dedechandran.domain.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class InsertFavoriteMovieUseCaseTest {
    @MockK
    lateinit var movieRepository: MovieRepository

    private val scheduler = TrampolineSchedulerProvider()

    private lateinit var insertFavoriteMovieUseCase: InsertFavoriteMovieUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        insertFavoriteMovieUseCase = InsertFavoriteMovieUseCase(
            movieRepository = movieRepository,
            baseSchedulerProvider = scheduler
        )
    }

    @Test
    fun buildUseCaseObservable_inUseCase_shouldProduce_observableOfUnit_when_successToInsertData() {
        val mockId = 1
        val mockTitle = "Title"
        val mockOverview = "MockOverview"
        val mockImgUrl = "ImgUrl"
        val mockReleaseDate = "ReleaseDate"
        every {
            movieRepository.insertFavoriteMovie(
                id = mockId,
                title = mockTitle,
                overview = mockOverview,
                imgUrl = mockImgUrl,
                releaseDate = mockReleaseDate
            )
        } returns Observable.just(Unit)

        insertFavoriteMovieUseCase.buildUseCaseObservable(
            params = InsertFavoriteMovieUseCase.Params(
                id = mockId,
                title = mockTitle,
                overview = mockOverview,
                imgUrl = mockImgUrl,
                releaseDate = mockReleaseDate
            )
        )
            .test()
            .assertResult(Unit)
            .assertComplete()
            .dispose()
        verify {
            movieRepository.insertFavoriteMovie(
                id = mockId,
                title = mockTitle,
                overview = mockOverview,
                imgUrl = mockImgUrl,
                releaseDate = mockReleaseDate
            )
        }
    }
}
