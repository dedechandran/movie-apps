package com.dedechandran.domain.usecase

import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.repository.MovieRepository
import com.dedechandran.domain.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class GetFavoriteMoviesUseCaseTest {
    @MockK
    lateinit var movieRepository: MovieRepository

    private val scheduler = TrampolineSchedulerProvider()

    private lateinit var getFavoriteMoviesUseCase: GetFavoriteMoviesUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getFavoriteMoviesUseCase = GetFavoriteMoviesUseCase(
            movieRepository = movieRepository,
            baseSchedulerProvider = scheduler
        )
    }

    @Test
    fun buildUseCaseObservable_inUseCase_shouldProduce_observableOfFavoriteMovies() {
        val actualResult = FavoriteMovie(
            items = emptyList()
        )
        every { movieRepository.getCountOfFavoriteMovies() } returns Observable.just(actualResult.items.size)
        every { movieRepository.getFavoriteMovies(any(), any(), any(), any()) } returns Observable.just(actualResult)

        val mockParams = GetFavoriteMoviesUseCase.Params(
            page = 1
        )
        getFavoriteMoviesUseCase.buildUseCaseObservable(params = mockParams)
            .test()
            .assertResult(actualResult)
            .assertComplete()
            .dispose()
        verify { movieRepository.getCountOfFavoriteMovies() }
        verify { movieRepository.getFavoriteMovies(any(), any(), any(), any()) }
    }
}
