package com.dedechandran.domain.usecase

import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.repository.MovieRepository
import com.dedechandran.domain.scheduler.TrampolineSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class GetMovieDetailsUseCaseTest {
    @MockK
    lateinit var movieRepository: MovieRepository

    private val scheduler = TrampolineSchedulerProvider()

    private lateinit var getMovieDetailsUseCase: GetMovieDetailsUseCase

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        getMovieDetailsUseCase = GetMovieDetailsUseCase(
            movieRepository = movieRepository,
            baseSchedulerProvider = scheduler
        )
    }

    @Test
    fun buildUseCaseObservable_inUseCase_shouldProduce_observableOfMovieDetails() {
        val mockId = 1
        val actualResult = MovieDetails(
            id = mockId,
            title = "Title",
            overview = "Overview",
            imgUrl = "ImgUrl",
            releaseDate = "2021-12-12",
            genres = emptyList(),
            trailers = emptyList(),
            runtime = 100,
            voteAverage = 0.0
        )
        every { movieRepository.getMovieDetails(id = mockId) } returns Observable.just(actualResult)
        every { movieRepository.getMovieVideos(id = mockId) } returns Observable.just(emptyList())
        every { movieRepository.getFavoriteMovieById(id = mockId) } returns Observable.just(FavoriteMovie(items = emptyList()))

        getMovieDetailsUseCase.buildUseCaseObservable(params = GetMovieDetailsUseCase.Params(mockId))
            .test()
            .assertResult(actualResult)
            .assertComplete()
            .dispose()
        verify { movieRepository.getMovieDetails(id = mockId) }
        verify { movieRepository.getMovieVideos(id = mockId) }
        verify { movieRepository.getFavoriteMovieById(id = mockId) }
    }
}
