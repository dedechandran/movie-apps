package com.dedechandran.domain.base

import io.reactivex.rxjava3.observers.DisposableObserver

abstract class BaseObserver<T> : DisposableObserver<T>() {

    override fun onStart() {
        // Do Nothing
    }

    override fun onNext(t: T) {
        // Do Nothing
    }

    override fun onError(e: Throwable?) {
        // Do Nothing
    }

    override fun onComplete() {
        // Do Nothing
    }
}
