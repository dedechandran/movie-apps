package com.dedechandran.domain.base

import io.reactivex.rxjava3.core.Scheduler

interface BaseSchedulerProvider {
    val io: Scheduler
    val computation: Scheduler
    val main: Scheduler
}
