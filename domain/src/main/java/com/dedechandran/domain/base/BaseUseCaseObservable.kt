package com.dedechandran.domain.base

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.observers.DisposableObserver

abstract class BaseUseCaseObservable<T, Params>(private val baseSchedulerProvider: BaseSchedulerProvider) {
    private val disposable = CompositeDisposable()

    abstract fun buildUseCaseObservable(params: Params): Observable<T>

    fun execute(observer: DisposableObserver<T>, params: Params) {
        val observable = buildUseCaseObservable(params = params)
            .subscribeOn(baseSchedulerProvider.io)
            .observeOn(baseSchedulerProvider.main)
        disposable.add(observable.subscribeWith(observer))
    }

    fun dispose() {
        if (!disposable.isDisposed) {
            disposable.dispose()
        }
    }
}
