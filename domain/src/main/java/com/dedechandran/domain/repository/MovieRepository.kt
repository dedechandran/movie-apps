package com.dedechandran.domain.repository

import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.model.MovieVideo
import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.model.TopRatedMovie
import io.reactivex.rxjava3.core.Observable

interface MovieRepository {
    fun getPopularMovies(page: Int): Observable<PopularMovie>
    fun getTopRatedMovies(page: Int): Observable<TopRatedMovie>
    fun getMovieDetails(id: Int): Observable<MovieDetails>
    fun getMovieVideos(id: Int): Observable<List<MovieVideo>>
    fun getFavoriteMovies(page: Int, limit: Int, offset: Int, isLastPage: Boolean): Observable<FavoriteMovie>
    fun getCountOfFavoriteMovies(): Observable<Int>
    fun insertFavoriteMovie(
        id: Int,
        title: String,
        overview: String,
        imgUrl: String,
        releaseDate: String
    ): Observable<Unit>
    fun deleteFavoriteMovie(
        id: Int,
        title: String,
        overview: String,
        imgUrl: String,
        releaseDate: String
    ): Observable<Unit>
    fun getFavoriteMovieById(id: Int): Observable<FavoriteMovie>
}
