package com.dedechandran.domain.model

data class TopRatedMovie(
    val page: Int,
    val isLastPage: Boolean,
    val items: List<TopRatedMovieItem>
) {
    data class TopRatedMovieItem(
        val id: Int,
        val imgUrl: String,
        val title: String,
        val overview: String,
        val releaseDate: String
    )
}
