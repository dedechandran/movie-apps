package com.dedechandran.domain.model

data class MovieVideo(
    val type: String,
    val name: String,
    val key: String,
    val site: String
)
