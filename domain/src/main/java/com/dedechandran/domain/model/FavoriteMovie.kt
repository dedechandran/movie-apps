package com.dedechandran.domain.model

data class FavoriteMovie(
    val page: Int = 1,
    val isLastPage: Boolean = true,
    val items: List<FavoriteMovieItem>
) {
    data class FavoriteMovieItem(
        val id: Int,
        val imgUrl: String,
        val title: String,
        val overview: String,
        val releaseDate: String
    )
}
