package com.dedechandran.domain.model

data class MovieDetails(
    val id: Int,
    val imgUrl: String,
    val title: String,
    val overview: String,
    val releaseDate: String,
    val genres: List<String>,
    val runtime: Int,
    val trailers: List<Trailer> = emptyList(),
    val voteAverage: Double,
    val isFavorite: Boolean = false
) {
    data class Trailer(
        val name: String,
        val key: String
    )
}
