package com.dedechandran.domain.model

data class PopularMovie(
    val page: Int,
    val isLastPage: Boolean,
    val items: List<PopularMovieItem>
) {
    data class PopularMovieItem(
        val id: Int,
        val imgUrl: String,
        val title: String,
        val overview: String,
        val releaseDate: String
    )
}
