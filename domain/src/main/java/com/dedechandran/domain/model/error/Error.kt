package com.dedechandran.domain.model.error

sealed class Error : Exception() {
    object NoNetworkException : Error()
    object InvalidApiKeyException : Error()
    object InternalServerErrorException : Error()
    object ResourceNotFoundException : Error()
    object UnknownErrorException : Error()
    object DatabaseException : Error()
}
