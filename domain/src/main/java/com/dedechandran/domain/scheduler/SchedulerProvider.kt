package com.dedechandran.domain.scheduler

import com.dedechandran.domain.base.BaseSchedulerProvider
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class SchedulerProvider @Inject constructor() : BaseSchedulerProvider {
    override val io: Scheduler
        get() = Schedulers.io()
    override val computation: Scheduler
        get() = Schedulers.computation()
    override val main: Scheduler
        get() = AndroidSchedulers.mainThread()
}
