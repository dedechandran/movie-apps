package com.dedechandran.domain.scheduler

import com.dedechandran.domain.base.BaseSchedulerProvider
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.TestScheduler

class TestSchedulerProvider constructor(private val testScheduler: TestScheduler) :
    BaseSchedulerProvider {
    override val io: Scheduler
        get() = testScheduler
    override val computation: Scheduler
        get() = testScheduler
    override val main: Scheduler
        get() = testScheduler
}
