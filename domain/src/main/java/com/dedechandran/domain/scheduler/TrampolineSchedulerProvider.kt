package com.dedechandran.domain.scheduler

import com.dedechandran.domain.base.BaseSchedulerProvider
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class TrampolineSchedulerProvider @Inject constructor() : BaseSchedulerProvider {
    override val io: Scheduler
        get() = Schedulers.trampoline()
    override val computation: Scheduler
        get() = Schedulers.trampoline()
    override val main: Scheduler
        get() = Schedulers.trampoline()
}
