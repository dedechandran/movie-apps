package com.dedechandran.domain.usecase

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.base.BaseUseCaseObservable
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class InsertFavoriteMovieUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    baseSchedulerProvider: BaseSchedulerProvider
) : BaseUseCaseObservable<Unit, InsertFavoriteMovieUseCase.Params>(baseSchedulerProvider) {

    override fun buildUseCaseObservable(params: Params): Observable<Unit> {
        return movieRepository.insertFavoriteMovie(
            id = params.id,
            title = params.title,
            overview = params.overview,
            imgUrl = params.imgUrl,
            releaseDate = params.releaseDate
        )
    }

    data class Params(
        val id: Int,
        val title: String,
        val overview: String,
        val imgUrl: String,
        val releaseDate: String
    )
}
