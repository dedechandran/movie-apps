package com.dedechandran.domain.usecase

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.base.BaseUseCaseObservable
import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject
import kotlin.math.ceil

class GetFavoriteMoviesUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    baseSchedulerProvider: BaseSchedulerProvider
) : BaseUseCaseObservable<FavoriteMovie, GetFavoriteMoviesUseCase.Params>(baseSchedulerProvider) {

    override fun buildUseCaseObservable(params: Params): Observable<FavoriteMovie> {
        return movieRepository.getCountOfFavoriteMovies()
            .flatMap { total ->
                val offset = (params.page - 1) * params.limit
                val availablePage = ceil(total.toDouble().div(params.page))
                movieRepository.getFavoriteMovies(
                    page = params.page,
                    limit = params.limit,
                    offset = offset,
                    isLastPage = params.page == availablePage.toInt()
                )
            }
    }

    data class Params(
        val page: Int,
        val limit: Int = 20
    )
}
