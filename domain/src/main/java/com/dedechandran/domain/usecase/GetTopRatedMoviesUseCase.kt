package com.dedechandran.domain.usecase

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.base.BaseUseCaseObservable
import com.dedechandran.domain.model.TopRatedMovie
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetTopRatedMoviesUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    baseSchedulerProvider: BaseSchedulerProvider
) : BaseUseCaseObservable<TopRatedMovie, GetTopRatedMoviesUseCase.Params>(baseSchedulerProvider) {

    override fun buildUseCaseObservable(params: Params): Observable<TopRatedMovie> {
        return movieRepository.getTopRatedMovies(page = params.page)
    }

    data class Params(
        val page: Int
    )
}
