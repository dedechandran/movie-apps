package com.dedechandran.domain.usecase

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.base.BaseUseCaseObservable
import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetPopularMoviesUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    baseSchedulerProvider: BaseSchedulerProvider
) : BaseUseCaseObservable<PopularMovie, GetPopularMoviesUseCase.Params>(baseSchedulerProvider) {

    override fun buildUseCaseObservable(params: Params): Observable<PopularMovie> {
        return movieRepository.getPopularMovies(page = params.page)
    }

    data class Params(
        val page: Int
    )
}
