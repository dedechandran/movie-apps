package com.dedechandran.domain.usecase

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.base.BaseUseCaseObservable
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class GetMovieDetailsUseCase @Inject constructor(
    private val movieRepository: MovieRepository,
    baseSchedulerProvider: BaseSchedulerProvider
) :
    BaseUseCaseObservable<MovieDetails, GetMovieDetailsUseCase.Params>(baseSchedulerProvider) {

    override fun buildUseCaseObservable(params: Params): Observable<MovieDetails> {
        return movieRepository.getMovieDetails(id = params.id)
            .flatMap { details ->
                movieRepository.getMovieVideos(id = params.id)
                    .map { trailers ->
                        details.copy(
                            trailers = trailers
                                .filter {
                                    it.type.uppercase() == "TRAILER" && it.site.uppercase() == "YOUTUBE"
                                }
                                .map {
                                    MovieDetails.Trailer(
                                        name = it.name,
                                        key = it.key
                                    )
                                }
                        )
                    }
            }
            .flatMap { details ->
                movieRepository.getFavoriteMovieById(id = details.id)
                    .map {
                        if (it.items.isEmpty()) {
                            details
                        } else {
                            details.copy(
                                isFavorite = true
                            )
                        }
                    }
            }
    }

    data class Params(
        val id: Int
    )
}
