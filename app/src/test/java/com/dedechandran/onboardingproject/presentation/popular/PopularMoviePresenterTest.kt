package com.dedechandran.onboardingproject.presentation.popular

import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.model.error.Error
import com.dedechandran.domain.usecase.GetPopularMoviesUseCase
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.justRun
import io.mockk.slot
import io.mockk.verify
import io.mockk.verifyOrder
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.observers.DisposableObserver
import org.junit.Before
import org.junit.Test

class PopularMoviePresenterTest {
    @MockK
    lateinit var errorMessageFactory: ErrorMessageFactory

    @MockK
    lateinit var view: PopularMovieView

    @MockK
    lateinit var getPopularMoviesUseCase: GetPopularMoviesUseCase

    private lateinit var presenter: PopularMoviePresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = PopularMoviePresenter(
            getPopularMoviesUseCase = getPopularMoviesUseCase,
            errorMessageFactory = errorMessageFactory,
            view = view
        )
    }

    @Test
    fun initialize_inPresenter_shouldSuccess_toShowPopularMovies_inView() {
        val mockData = PopularMovie(
            page = 1,
            isLastPage = true,
            items = emptyList()
        )
        val argumentCapture = slot<DisposableObserver<PopularMovie>>()
        justRun { getPopularMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onNext(mockData)
        observer.onComplete()
        verifyOrder {
            view.showLoadingIndicator(isLoading = true)
            view.showPopularMovieItems(emptyList())
            view.showLoadingIndicator(isLoading = false)
        }
    }

    @Test
    fun initialize_inPresenter_shouldFailed_toShowPopularMovies_inView() {
        val argumentCapture = slot<DisposableObserver<PopularMovie>>()
        val mockErrorMessage = "No Network"
        every { errorMessageFactory.create(Error.NoNetworkException) } returns mockErrorMessage
        justRun { getPopularMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onError(Error.NoNetworkException)
        verifyOrder {
            view.showLoadingIndicator(isLoading = true)
            view.showLoadingIndicator(
                isLoading = false,
                isError = mockErrorMessage.isNotEmpty()
            )
            view.showError(message = mockErrorMessage, isLoading = false)
        }
    }

    @Test
    fun onLoadMorePopularMovies_inPresenter_shouldSuccess_toShowMorePopularMovies_inView() {
        val mockData = PopularMovie(page = 1, isLastPage = false, items = emptyList())
        val loadMoreMockData = PopularMovie(
            page = 2,
            isLastPage = true,
            items = listOf(
                PopularMovie.PopularMovieItem(
                    id = 1,
                    title = "Title",
                    overview = "Overview",
                    releaseDate = "2021-02-24",
                    imgUrl = "imgUrl"
                )
            )
        )
        val argumentCapture = mutableListOf<DisposableObserver<PopularMovie>>()
        justRun { getPopularMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()
        presenter.onLoadMorePopularMovies(2)

        val firstLoadObserver = argumentCapture.first()
        firstLoadObserver.onSubscribe(Disposable.empty())
        firstLoadObserver.onNext(mockData)
        firstLoadObserver.onComplete()
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showPopularMovieItems(any())
            view.setPagination(1)
            view.setIsLastPage(false)
            view.showLoadingIndicator(false)
        }

        val secondLoadObserver = argumentCapture.last()
        secondLoadObserver.onSubscribe(Disposable.empty())
        secondLoadObserver.onNext(loadMoreMockData)
        secondLoadObserver.onComplete()
        verifyOrder {
            view.showLoadMoreIndicator(true)
            view.showPopularMovieItems(any())
            view.setPagination(2)
            view.setIsLastPage(true)
            view.showLoadMoreIndicator(false)
        }
    }

    @Test
    fun onLoadMorePopularMovies_inPresenter_shouldFailed_toShowMorePopularMovies_inView() {
        val mockData = PopularMovie(page = 1, isLastPage = false, items = emptyList())
        val mockErrorMessage = "No Network"
        every { errorMessageFactory.create(Error.NoNetworkException) } returns mockErrorMessage

        val argumentCapture = mutableListOf<DisposableObserver<PopularMovie>>()
        justRun { getPopularMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()
        presenter.onLoadMorePopularMovies(2)

        val firstLoadObserver = argumentCapture.first()
        firstLoadObserver.onSubscribe(Disposable.empty())
        firstLoadObserver.onNext(mockData)
        firstLoadObserver.onComplete()
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showPopularMovieItems(any())
            view.setPagination(1)
            view.setIsLastPage(false)
            view.showLoadingIndicator(false)
        }

        val secondLoadObserver = argumentCapture.last()
        secondLoadObserver.onSubscribe(Disposable.empty())
        secondLoadObserver.onError(Error.NoNetworkException)
        secondLoadObserver.onComplete()
        verifyOrder {
            view.showLoadMoreIndicator(isLoading = true)
            view.showLoadMoreIndicator(isLoading = false)
            view.showSnackBar(mockErrorMessage)
        }
    }

    @Test
    fun onItemClicked_inPresenter_shouldCall_navigateToMovieDetails_inView() {
        val mockId = 1
        presenter.onItemClicked(mockId)
        verify { view.navigateToMovieDetails(id = mockId) }
    }
}
