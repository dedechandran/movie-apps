package com.dedechandran.onboardingproject.presentation.splash

import com.dedechandran.domain.scheduler.TestSchedulerProvider
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit

class SplashPresenterTest {

    @MockK
    lateinit var view: SplashView

    private val testScheduler = TestScheduler()
    private val testSchedulerProvider = TestSchedulerProvider(testScheduler)

    private lateinit var presenter: SplashPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = SplashPresenter(testSchedulerProvider, view)
    }

    @Test
    fun initialize_inPresenter_shouldCall_navigateToHomeScreen_inView() {
        presenter.initialize()
        testScheduler.advanceTimeBy(2000L, TimeUnit.MILLISECONDS)
        verify { view.navigateToHomeScreen() }
    }
}
