package com.dedechandran.onboardingproject.presentation.favorite

import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.model.error.Error
import com.dedechandran.domain.usecase.GetFavoriteMoviesUseCase
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.justRun
import io.mockk.slot
import io.mockk.verify
import io.mockk.verifyOrder
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.observers.DisposableObserver
import org.junit.Before
import org.junit.Test

class FavoriteMoviePresenterTest {
    @MockK
    lateinit var errorMessageFactory: ErrorMessageFactory

    @MockK
    lateinit var view: FavoriteMovieView

    @MockK
    lateinit var getFavoriteMoviesUseCase: GetFavoriteMoviesUseCase

    private lateinit var presenter: FavoriteMoviePresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = FavoriteMoviePresenter(
            getFavoriteMoviesUseCase = getFavoriteMoviesUseCase,
            errorMessageFactory = errorMessageFactory,
            view = view
        )
    }

    @Test
    fun initialize_inPresenter_shouldSuccess_toShowFavoriteMovies_inView() {
        val mockData = FavoriteMovie(
            items = emptyList()
        )
        val argumentCapture = slot<DisposableObserver<FavoriteMovie>>()
        justRun { getFavoriteMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onNext(mockData)
        observer.onComplete()
        verifyOrder {
            view.showLoadingIndicator(isLoading = true)
            view.showFavoriteMovieItems(emptyList())
            view.showLoadingIndicator(isLoading = false)
        }
    }

    @Test
    fun initialize_inPresenter_shouldFailed_toShowFavoriteMovies_inView() {
        val argumentCapture = slot<DisposableObserver<FavoriteMovie>>()
        val mockErrorMessage = "No Network"
        every { errorMessageFactory.create(Error.NoNetworkException) } returns mockErrorMessage
        justRun { getFavoriteMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onError(Error.NoNetworkException)
        verifyOrder {
            view.showLoadingIndicator(isLoading = true)
            view.showLoadingIndicator(
                isLoading = false,
                isError = mockErrorMessage.isNotEmpty()
            )
            view.showError(message = mockErrorMessage, isLoading = false)
        }
    }

    @Test
    fun onLoadMoreFavoriteMovies_inPresenter_shouldSuccess_toShowMoreFavoriteMovies_inView() {
        val mockData = FavoriteMovie(page = 1, isLastPage = false, items = emptyList())
        val loadMoreMockData = FavoriteMovie(
            page = 2,
            isLastPage = true,
            items = listOf(
                FavoriteMovie.FavoriteMovieItem(
                    id = 1,
                    title = "Title",
                    overview = "Overview",
                    releaseDate = "2021-02-24",
                    imgUrl = "imgUrl"
                )
            )
        )
        val argumentCapture = mutableListOf<DisposableObserver<FavoriteMovie>>()
        justRun { getFavoriteMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()
        presenter.onLoadMoreFavoriteMovies(2)

        val firstLoadObserver = argumentCapture.first()
        firstLoadObserver.onSubscribe(Disposable.empty())
        firstLoadObserver.onNext(mockData)
        firstLoadObserver.onComplete()
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showFavoriteMovieItems(any())
            view.setPagination(1)
            view.setIsLastPage(false)
            view.showLoadingIndicator(false)
        }

        val secondLoadObserver = argumentCapture.last()
        secondLoadObserver.onSubscribe(Disposable.empty())
        secondLoadObserver.onNext(loadMoreMockData)
        secondLoadObserver.onComplete()
        verifyOrder {
            view.showLoadMoreIndicator(true)
            view.showFavoriteMovieItems(any())
            view.setPagination(2)
            view.setIsLastPage(true)
            view.showLoadMoreIndicator(false)
        }
    }

    @Test
    fun onLoadMoreFavoriteMovies_inPresenter_shouldFailed_toShowMoreFavoriteMovies_inView() {
        val mockData = FavoriteMovie(page = 1, isLastPage = false, items = emptyList())
        val mockErrorMessage = "No Network"
        every { errorMessageFactory.create(Error.NoNetworkException) } returns mockErrorMessage

        val argumentCapture = mutableListOf<DisposableObserver<FavoriteMovie>>()
        justRun { getFavoriteMoviesUseCase.execute(capture(argumentCapture), any()) }

        presenter.initialize()
        presenter.onLoadMoreFavoriteMovies(2)

        val firstLoadObserver = argumentCapture.first()
        firstLoadObserver.onSubscribe(Disposable.empty())
        firstLoadObserver.onNext(mockData)
        firstLoadObserver.onComplete()
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showFavoriteMovieItems(any())
            view.setPagination(1)
            view.setIsLastPage(false)
            view.showLoadingIndicator(false)
        }

        val secondLoadObserver = argumentCapture.last()
        secondLoadObserver.onSubscribe(Disposable.empty())
        secondLoadObserver.onError(Error.NoNetworkException)
        secondLoadObserver.onComplete()
        verifyOrder {
            view.showLoadMoreIndicator(isLoading = true)
            view.showLoadMoreIndicator(isLoading = false)
            view.showSnackBar(mockErrorMessage)
        }
    }

    @Test
    fun onItemClicked_inPresenter_shouldCall_navigateToMovieDetails_inView() {
        val mockId = 1
        presenter.onItemClicked(mockId)
        verify { view.navigateToMovieDetails(id = mockId) }
    }
}
