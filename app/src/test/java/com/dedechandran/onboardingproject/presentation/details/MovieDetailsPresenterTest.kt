package com.dedechandran.onboardingproject.presentation.details

import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.model.error.Error
import com.dedechandran.domain.usecase.DeleteFavoriteMovieUseCase
import com.dedechandran.domain.usecase.GetMovieDetailsUseCase
import com.dedechandran.domain.usecase.InsertFavoriteMovieUseCase
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import com.dedechandran.onboardingproject.util.displayAsHyphenIfEmpty
import com.dedechandran.onboardingproject.util.formatDate
import com.dedechandran.onboardingproject.util.manager.ResourceManager
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.justRun
import io.mockk.slot
import io.mockk.verify
import io.mockk.verifyOrder
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.observers.DisposableObserver
import org.junit.Before
import org.junit.Test

class MovieDetailsPresenterTest {
    @MockK
    lateinit var getMovieDetailsUseCase: GetMovieDetailsUseCase

    @MockK
    lateinit var deleteFavoriteMovieUseCase: DeleteFavoriteMovieUseCase

    @MockK
    lateinit var insertFavoriteMovieUseCase: InsertFavoriteMovieUseCase

    @MockK
    lateinit var resourceManager: ResourceManager

    @MockK
    lateinit var errorMessageFactory: ErrorMessageFactory

    @MockK
    lateinit var view: MovieDetailsView

    private lateinit var presenter: MovieDetailsPresenter

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        presenter = MovieDetailsPresenter(
            getMovieDetailsUseCase = getMovieDetailsUseCase,
            deleteFavoriteMovieUseCase = deleteFavoriteMovieUseCase,
            insertFavoriteMovieUseCase = insertFavoriteMovieUseCase,
            errorMessageFactory = errorMessageFactory,
            resourceManager = resourceManager,
            view = view
        )
    }

    @Test
    fun initialize_inPresenter_shouldSuccess_toShowMovieDetails_inView() {
        val argumentCapture = slot<DisposableObserver<MovieDetails>>()
        justRun { getMovieDetailsUseCase.execute(capture(argumentCapture), any()) }

        val mockId = 1
        val mockData = MovieDetails(
            id = mockId,
            title = "Title",
            overview = "Overview",
            imgUrl = "ImgUrl",
            releaseDate = "2021-12-12",
            genres = emptyList(),
            trailers = emptyList(),
            runtime = 100,
            voteAverage = 0.0
        )
        val mockDisplayedItem = mockData.copy(
            releaseDate = if (mockData.releaseDate.isEmpty()) {
                mockData.releaseDate.displayAsHyphenIfEmpty()
            } else {
                mockData.releaseDate.formatDate()
            },
            title = mockData.title.displayAsHyphenIfEmpty(),
            overview = mockData.overview.displayAsHyphenIfEmpty(),
        )
        presenter.initialize(mockId)

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onNext(mockData)
        observer.onComplete()
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showMovieDetails(mockDisplayedItem)
            view.showLoadingIndicator(false)
        }
    }

    @Test
    fun initialize_inPresenter_shouldFailed_toShowMovieDetails_inView() {
        val argumentCapture = slot<DisposableObserver<MovieDetails>>()
        val mockErrorMessage = "No Network"
        justRun { getMovieDetailsUseCase.execute(capture(argumentCapture), any()) }
        every { errorMessageFactory.create(Error.NoNetworkException) } returns mockErrorMessage

        val mockId = 1
        presenter.initialize(mockId)

        val observer = argumentCapture.captured
        observer.onSubscribe(Disposable.empty())
        observer.onError(Error.NoNetworkException)
        verifyOrder {
            view.showLoadingIndicator(true)
            view.showLoadingIndicator(isLoading = false, isError = mockErrorMessage.isNotEmpty())
            view.showError(mockErrorMessage)
        }
    }

    @Test
    fun onFavoriteIconClicked_inPresenter_shouldSuccess_toInsertFavoriteMovie_inView() {
        val argumentCapture = slot<DisposableObserver<Unit>>()
        val successfullyToAddFavoriteMessage = "Added to favorite"
        justRun { insertFavoriteMovieUseCase.execute(capture(argumentCapture), any()) }
        every {
            resourceManager.getString(R.string.details_successfully_to_add_favorite_movie_snackbar_message)
        } returns successfullyToAddFavoriteMessage

        presenter.onFavoriteIconClicked(true)

        val observer = argumentCapture.captured
        observer.onComplete()
        verify { view.showSnackBar(successfullyToAddFavoriteMessage) }
        verify { view.updateFavoriteIcon(true) }
    }

    @Test
    fun onFavoriteIconClicked_inPresenter_shouldFailed_toInsertFavoriteMovie_inView() {
        val argumentCapture = slot<DisposableObserver<Unit>>()
        val mockErrorMessage = "Something went wrong"
        justRun { insertFavoriteMovieUseCase.execute(capture(argumentCapture), any()) }
        every { errorMessageFactory.create(Error.UnknownErrorException) } returns mockErrorMessage

        presenter.onFavoriteIconClicked(true)

        val observer = argumentCapture.captured
        observer.onError(Error.UnknownErrorException)
        verify { view.showSnackBar(mockErrorMessage) }
    }

    @Test
    fun onFavoriteIconClicked_inPresenter_shouldSuccess_toDeleteFavoriteMovie_inView() {
        val argumentCapture = slot<DisposableObserver<Unit>>()
        val successfullyToRemoveFavoriteMessage = "Removed from favorite"
        justRun { deleteFavoriteMovieUseCase.execute(capture(argumentCapture), any()) }
        every {
            resourceManager.getString(R.string.details_successfully_to_remove_favorite_movie_snackbar_message)
        } returns successfullyToRemoveFavoriteMessage

        presenter.onFavoriteIconClicked(false)

        val observer = argumentCapture.captured
        observer.onComplete()
        verify { view.showSnackBar(successfullyToRemoveFavoriteMessage) }
        verify { view.updateFavoriteIcon(false) }
    }

    @Test
    fun onFavoriteIconClicked_inPresenter_shouldFailed_toDeleteFavoriteMovie_inView() {
        val argumentCapture = slot<DisposableObserver<Unit>>()
        val mockErrorMessage = "Something went wrong"
        justRun { deleteFavoriteMovieUseCase.execute(capture(argumentCapture), any()) }
        every { errorMessageFactory.create(Error.UnknownErrorException) } returns mockErrorMessage

        presenter.onFavoriteIconClicked(false)

        val observer = argumentCapture.captured
        observer.onError(Error.UnknownErrorException)
        verify { view.showSnackBar(mockErrorMessage) }
    }

    @Test
    fun onTrailerItemClicked_inPresenter_shouldCall_navigateToYoutubeApp_inView() {
        val mockTrailerKey = "123xyz"
        presenter.onTrailerItemClicked(mockTrailerKey)

        verify { view.navigateToYoutubeApp(mockTrailerKey) }
    }
}
