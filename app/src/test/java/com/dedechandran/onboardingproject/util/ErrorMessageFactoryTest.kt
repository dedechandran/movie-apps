package com.dedechandran.onboardingproject.util

import com.dedechandran.domain.model.error.Error
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.util.manager.ResourceManager
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ErrorMessageFactoryTest {
    @MockK
    lateinit var resourceManager: ResourceManager

    private lateinit var errorMessageFactory: ErrorMessageFactory

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        errorMessageFactory = ErrorMessageFactory(resourceManager)
    }

    @Test
    fun create_inFactory_shouldProduce_noNetworkExceptionErrorMessage() {
        val actualErrorMessage = "No network connection"
        every { resourceManager.getString(R.string.no_network_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.NoNetworkException)
        assertEquals(errorMessage, actualErrorMessage)
    }

    @Test
    fun create_inFactory_shouldProduce_resourceNotFoundExceptionErrorMessage() {
        val actualErrorMessage = "Resource not found"
        every { resourceManager.getString(R.string.no_resource_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.ResourceNotFoundException)
        assertEquals(errorMessage, actualErrorMessage)
    }

    @Test
    fun create_inFactory_shouldProduce_invalidApiKeyExceptionErrorMessage() {
        val actualErrorMessage = "Invalid API key"
        every { resourceManager.getString(R.string.invalid_api_key_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.InvalidApiKeyException)
        assertEquals(errorMessage, actualErrorMessage)
    }

    @Test
    fun create_inFactory_shouldProduce_unknownExceptionErrorMessage() {
        val actualErrorMessage = "Something went wrong"
        every { resourceManager.getString(R.string.unknown_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.UnknownErrorException)
        assertEquals(errorMessage, actualErrorMessage)
    }

    @Test
    fun create_inFactory_shouldProduce_internalServerExceptionErrorMessage() {
        val actualErrorMessage = "System is busy at the moment"
        every { resourceManager.getString(R.string.server_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.InternalServerErrorException)
        assertEquals(errorMessage, actualErrorMessage)
    }

    @Test
    fun create_inFactory_shouldProduce_DatabaseExceptionErrorMessage() {
        val actualErrorMessage = "Something went wrong"
        every { resourceManager.getString(R.string.unknown_error_message) } returns actualErrorMessage
        val errorMessage = errorMessageFactory.create(Error.DatabaseException)
        assertEquals(errorMessage, actualErrorMessage)
    }
}
