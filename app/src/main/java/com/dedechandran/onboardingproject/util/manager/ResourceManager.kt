package com.dedechandran.onboardingproject.util.manager

interface ResourceManager {
    fun getString(id: Int): String
}
