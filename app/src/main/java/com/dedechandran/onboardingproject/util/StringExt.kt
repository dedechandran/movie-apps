package com.dedechandran.onboardingproject.util

import java.text.SimpleDateFormat
import java.util.Locale

fun String.formatDate(from: String = "yyyy-MM-dd", to: String = "dd MMMM yyyy"): String {
    val localDatePattern = SimpleDateFormat(from, Locale.getDefault())
    val localDate = localDatePattern.parse(this)
    val formattedDate = SimpleDateFormat(to, Locale.getDefault())
    return formattedDate.format(localDate)
}

fun String.displayAsHyphenIfEmpty(): String {
    return if (this.isEmpty()) "-" else this
}
