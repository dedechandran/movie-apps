package com.dedechandran.onboardingproject.util

import com.dedechandran.domain.model.error.Error
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.util.manager.ResourceManager
import javax.inject.Inject

class ErrorMessageFactory @Inject constructor(
    private val resourceManager: ResourceManager
) {
    fun create(t: Throwable?): String {
        return when (t) {
            is Error.NoNetworkException -> {
                resourceManager.getString(R.string.no_network_error_message)
            }
            is Error.InvalidApiKeyException -> {
                resourceManager.getString(R.string.invalid_api_key_error_message)
            }
            is Error.ResourceNotFoundException -> {
                resourceManager.getString(R.string.no_resource_error_message)
            }
            is Error.InternalServerErrorException -> {
                resourceManager.getString(R.string.server_error_message)
            }
            is Error.UnknownErrorException -> {
                resourceManager.getString(R.string.unknown_error_message)
            }
            is Error.DatabaseException -> {
                resourceManager.getString(R.string.unknown_error_message)
            }
            else -> {
                resourceManager.getString(R.string.unknown_error_message)
            }
        }
    }
}
