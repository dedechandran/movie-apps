package com.dedechandran.onboardingproject.presentation.details

import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.onboardingproject.base.BaseView

interface MovieDetailsView : BaseView {
    fun showMovieDetails(movieDetails: MovieDetails)
    fun navigateToYoutubeApp(key: String)
    fun updateFavoriteIcon(isFavorite: Boolean)
}
