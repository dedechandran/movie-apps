package com.dedechandran.onboardingproject.presentation.splash

import com.dedechandran.onboardingproject.base.BaseView

interface SplashView : BaseView {
    override fun showSnackBar(message: String) {
        throw UnsupportedOperationException()
    }

    override fun showLoadingIndicator(isLoading: Boolean, isError: Boolean, hasData: Boolean) {
        throw UnsupportedOperationException()
    }

    override fun showError(message: String, isLoading: Boolean) {
        throw UnsupportedOperationException()
    }

    fun navigateToHomeScreen()
}
