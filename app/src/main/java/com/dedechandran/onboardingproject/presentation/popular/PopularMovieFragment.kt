package com.dedechandran.onboardingproject.presentation.popular

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseFragment
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.makeSnackBar
import com.google.android.material.button.MaterialButton
import javax.inject.Inject

class PopularMovieFragment : BaseFragment(), PopularMovieView {

    @Inject
    lateinit var presenter: PopularMoviePresenter

    private lateinit var toolbar: Toolbar
    private lateinit var rvPopularMovies: MovieRecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var constraintErrorState: ConstraintLayout
    private lateinit var tvErrorRemark: TextView
    private lateinit var btnRetry: MaterialButton
    private lateinit var coordinatorPopularMovies: CoordinatorLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_popular_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initFragment()
    }

    private fun initView(view: View) {
        with(view) {
            toolbar = findViewById(R.id.toolbar)
            rvPopularMovies = findViewById(R.id.rv_popular_movies)
            progressBar = findViewById(R.id.progress_bar)
            constraintErrorState = findViewById(R.id.constraint_error_state)
            tvErrorRemark = constraintErrorState.findViewById(R.id.tv_error_remark)
            btnRetry = constraintErrorState.findViewById(R.id.btn_retry)
            coordinatorPopularMovies = findViewById(R.id.coordinator_popular_movies)

            (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)

            rvPopularMovies.apply {
                setOnLoadMoreListener {
                    presenter.onLoadMorePopularMovies(it.inc())
                }
                setOnItemClickListener {
                    presenter.onItemClicked(it)
                }
                setHasFixedSize(true)
            }
        }
    }

    private fun initFragment() {
        presenter.initialize()
    }

    override fun showPopularMovieItems(items: List<MovieRecyclerViewItem>) {
        rvPopularMovies.setItems(items = items)
    }

    override fun showLoadMoreIndicator(isLoading: Boolean) {
        rvPopularMovies.setIsLoading(isLoading)
    }

    override fun setPagination(page: Int) {
        rvPopularMovies.setPagination(page = page)
    }

    override fun setIsLastPage(isLastPage: Boolean) {
        rvPopularMovies.setIsLastPage(isLastPage = isLastPage)
    }

    override fun navigateToMovieDetails(id: Int) {
        navigator.navigateToMovieDetails(context = requireContext(), movieId = id)
    }

    override fun showSnackBar(message: String) {
        requireContext().makeSnackBar(message = message, view = coordinatorPopularMovies)
    }

    override fun showLoadingIndicator(isLoading: Boolean, isError: Boolean, hasData: Boolean) {
        progressBar.isVisible = isLoading
    }

    override fun showError(message: String, isLoading: Boolean) {
        constraintErrorState.isVisible = message.isNotEmpty()
        tvErrorRemark.text = message
        btnRetry.setOnClickListener {
            presenter.initialize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
