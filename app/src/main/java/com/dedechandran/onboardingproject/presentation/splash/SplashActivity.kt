package com.dedechandran.onboardingproject.presentation.splash

import android.os.Bundle
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseActivity
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashView {

    @Inject lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.initialize()
    }

    override fun navigateToHomeScreen() {
        navigator.navigateToHomeScreen(context = this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
