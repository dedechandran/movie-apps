package com.dedechandran.onboardingproject.presentation.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import coil.load
import coil.transform.RoundedCornersTransformation
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseActivity
import com.dedechandran.onboardingproject.base.ui.trailer.TrailerRecyclerView
import com.dedechandran.onboardingproject.base.ui.trailer.TrailerRecyclerViewItem
import com.dedechandran.onboardingproject.util.getDurationDisplayedItem
import com.dedechandran.onboardingproject.util.makeSnackBar
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.button.MaterialButton
import com.google.android.material.chip.Chip
import javax.inject.Inject

class MovieDetailsActivity : BaseActivity(), MovieDetailsView {

    @Inject
    lateinit var presenter: MovieDetailsPresenter

    private var movieId: Int? = null
    private lateinit var coordinatorMovieDetails: CoordinatorLayout
    private lateinit var toolbar: Toolbar
    private lateinit var tvMovieDetailsTitle: TextView
    private lateinit var ivMovieDetailsImage: ImageView
    private lateinit var tvMovieDetailsReleaseDate: TextView
    private lateinit var tvMovieDetailsDuration: TextView
    private lateinit var tvMovieDetailsRating: TextView
    private lateinit var flexboxMovieDetailsGenres: FlexboxLayout
    private lateinit var tvMovieDetailsOverview: TextView
    private lateinit var rvMovieDetailsTrailers: TrailerRecyclerView
    private lateinit var nsvContent: NestedScrollView
    private lateinit var progressBar: ProgressBar
    private lateinit var constraintErrorState: ConstraintLayout
    private lateinit var tvErrorRemark: TextView
    private lateinit var btnRetry: MaterialButton
    private var menuFavorite: MenuItem? = null
    private var isFavorite: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        initView()
        initActivity()
    }

    private fun initActivity() {
        movieId = intent.getIntExtra(INTENT_EXTRA_PARAM_MOVIE_ID, -1)
        movieId?.let {
            presenter.initialize(id = it)
        }
    }

    private fun initView() {
        coordinatorMovieDetails = findViewById(R.id.coordinator_movie_details)
        toolbar = findViewById(R.id.toolbar)
        tvMovieDetailsTitle = findViewById(R.id.tv_movie_details_title)
        ivMovieDetailsImage = findViewById(R.id.iv_movie_details_image)
        tvMovieDetailsReleaseDate = findViewById(R.id.tv_movie_details_release_date)
        tvMovieDetailsDuration = findViewById(R.id.tv_movie_details_duration)
        tvMovieDetailsRating = findViewById(R.id.tv_movie_details_rating)
        flexboxMovieDetailsGenres = findViewById(R.id.flexbox_movie_details_genres)
        tvMovieDetailsOverview = findViewById(R.id.tv_movie_details_overview)
        rvMovieDetailsTrailers = findViewById(R.id.rv_movie_details_trailers)
        nsvContent = findViewById(R.id.nsv_content)
        progressBar = findViewById(R.id.progress_bar)
        constraintErrorState = findViewById(R.id.constraint_error_state)
        tvErrorRemark = constraintErrorState.findViewById(R.id.tv_error_remark)
        btnRetry = constraintErrorState.findViewById(R.id.btn_retry)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        rvMovieDetailsTrailers.apply {
            setOnItemClickListener {
                presenter.onTrailerItemClicked(key = it)
            }
        }
    }

    override fun showMovieDetails(movieDetails: MovieDetails) {
        with(movieDetails) {
            updateFavoriteIcon(movieDetails.isFavorite)
            tvMovieDetailsTitle.text = title
            ivMovieDetailsImage.load(imgUrl) {
                crossfade(true)
                transformations(RoundedCornersTransformation(16F))
            }
            tvMovieDetailsReleaseDate.text = releaseDate
            tvMovieDetailsDuration.text = runtime.getDurationDisplayedItem()
            tvMovieDetailsRating.text = "$voteAverage"
            tvMovieDetailsOverview.text = overview
            bindGenres(genres = genres)
            val trailerDisplayedItems = trailers.map {
                TrailerRecyclerViewItem(
                    name = it.name,
                    key = it.key
                )
            }
            rvMovieDetailsTrailers.setItems(trailerDisplayedItems)
        }
    }

    override fun navigateToYoutubeApp(key: String) {
        navigator.navigateToYoutubeApp(context = this, key = key)
    }

    override fun updateFavoriteIcon(isFavorite: Boolean) {
        this.isFavorite = isFavorite
        if (isFavorite) {
            menuFavorite?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_favorite_24)
        } else {
            menuFavorite?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_favorite_border_24)
        }
    }

    private fun bindGenres(genres: List<String>) {
        val genreLayoutParam = FlexboxLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        ).apply {
            setMargins(0, 0, 8, 0)
        }
        genres.forEach {
            val genre = Chip(this).apply {
                text = it
                layoutParams = genreLayoutParam
            }
            flexboxMovieDetailsGenres.addView(genre)
        }
    }

    override fun showSnackBar(message: String) {
        makeSnackBar(message = message, view = coordinatorMovieDetails)
    }

    override fun showLoadingIndicator(isLoading: Boolean, isError: Boolean, hasData: Boolean) {
        progressBar.isVisible = isLoading
        nsvContent.isVisible = !isLoading && !isError
        menuFavorite?.isEnabled = !isLoading && !isError
    }

    override fun showError(message: String, isLoading: Boolean) {
        constraintErrorState.isVisible = message.isNotEmpty()
        tvErrorRemark.text = message
        btnRetry.setOnClickListener {
            movieId?.let {
                presenter.initialize(id = it)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_movie_details_toolbar, menu)
        menuFavorite = menu?.findItem(R.id.menu_details_favorite)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
            R.id.menu_details_favorite -> presenter.onFavoriteIconClicked(!isFavorite)
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun getIntent(context: Context, movieId: Int): Intent {
            return Intent(context, MovieDetailsActivity::class.java).apply {
                putExtra(INTENT_EXTRA_PARAM_MOVIE_ID, movieId)
            }
        }

        private const val INTENT_EXTRA_PARAM_MOVIE_ID = "org.onboardingproject.INTENT_PARAM_MOVIE_ID"
    }
}
