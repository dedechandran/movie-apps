package com.dedechandran.onboardingproject.presentation.details

import dagger.Binds
import dagger.Module

@Module
abstract class MovieDetailsModule {
    @Binds
    abstract fun provideMovieDetailsView(movieDetailsActivity: MovieDetailsActivity): MovieDetailsView
}
