package com.dedechandran.onboardingproject.presentation.favorite

import com.dedechandran.onboardingproject.base.BaseView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem

interface FavoriteMovieView : BaseView {
    fun showFavoriteMovieItems(items: List<MovieRecyclerViewItem>)
    fun showLoadMoreIndicator(isLoading: Boolean)
    fun setPagination(page: Int)
    fun setIsLastPage(isLastPage: Boolean)
    fun navigateToMovieDetails(id: Int)
}
