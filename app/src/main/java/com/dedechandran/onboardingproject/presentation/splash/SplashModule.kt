package com.dedechandran.onboardingproject.presentation.splash

import dagger.Binds
import dagger.Module

@Module
abstract class SplashModule {

    @Binds
    abstract fun bindSplashView(splashActivity: SplashActivity): SplashView
}
