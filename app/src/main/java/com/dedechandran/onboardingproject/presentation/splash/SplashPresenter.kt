package com.dedechandran.onboardingproject.presentation.splash

import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.onboardingproject.base.BasePresenter
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleObserver
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SplashPresenter @Inject constructor(
    private val baseSchedulerProvider: BaseSchedulerProvider,
    private val view: SplashView
) : BasePresenter {

    private val compositeDisposable = CompositeDisposable()

    fun initialize() {
        Single.just(Unit)
            .delay(DELAY, TimeUnit.MILLISECONDS, baseSchedulerProvider.computation)
            .subscribeOn(baseSchedulerProvider.computation)
            .observeOn(baseSchedulerProvider.main)
            .subscribe(object : SingleObserver<Unit> {
                override fun onSubscribe(d: Disposable?) {
                    compositeDisposable.add(d)
                }

                override fun onSuccess(t: Unit?) {
                    view.navigateToHomeScreen()
                }

                override fun onError(e: Throwable?) {
                    // Do nothing
                }
            })
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    companion object {
        private const val DELAY = 2000L
    }
}
