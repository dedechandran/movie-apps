package com.dedechandran.onboardingproject.presentation.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseFragment
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.makeSnackBar
import com.google.android.material.button.MaterialButton
import javax.inject.Inject

class FavoriteMovieFragment : BaseFragment(), FavoriteMovieView {

    @Inject
    lateinit var presenter: FavoriteMoviePresenter

    private lateinit var toolbar: Toolbar
    private lateinit var rvFavoriteMovies: MovieRecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var constraintErrorState: ConstraintLayout
    private lateinit var tvErrorRemark: TextView
    private lateinit var btnRetry: MaterialButton
    private lateinit var coordinatorFavoriteMovies: CoordinatorLayout
    private lateinit var tvEmptyRemark: TextView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
    }

    override fun onResume() {
        super.onResume()
        refreshData()
    }

    private fun initView(view: View) {
        with(view) {
            toolbar = findViewById(R.id.toolbar)
            rvFavoriteMovies = findViewById(R.id.rv_favorite_movies)
            progressBar = findViewById(R.id.progress_bar)
            constraintErrorState = findViewById(R.id.constraint_error_state)
            tvErrorRemark = constraintErrorState.findViewById(R.id.tv_error_remark)
            btnRetry = constraintErrorState.findViewById(R.id.btn_retry)
            coordinatorFavoriteMovies = findViewById(R.id.coordinator_favorite_movies)
            tvEmptyRemark = findViewById(R.id.tv_empty_remark)

            (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)

            rvFavoriteMovies.apply {
                setOnItemClickListener {
                    presenter.onItemClicked(it)
                }
                setOnLoadMoreListener {
                    presenter.onLoadMoreFavoriteMovies(it.inc())
                }
                setHasFixedSize(true)
            }
        }
    }

    private fun refreshData() {
        presenter.clearData()
        initFragment()
    }

    private fun initFragment() {
        presenter.initialize()
    }

    override fun showFavoriteMovieItems(items: List<MovieRecyclerViewItem>) {
        rvFavoriteMovies.setItems(items)
    }

    override fun showLoadMoreIndicator(isLoading: Boolean) {
        rvFavoriteMovies.setIsLoading(isLoading)
    }

    override fun setPagination(page: Int) {
        rvFavoriteMovies.setPagination(page = page)
    }

    override fun setIsLastPage(isLastPage: Boolean) {
        rvFavoriteMovies.setIsLastPage(isLastPage = isLastPage)
    }

    override fun navigateToMovieDetails(id: Int) {
        navigator.navigateToMovieDetails(context = requireContext(), movieId = id)
    }

    override fun showSnackBar(message: String) {
        requireContext().makeSnackBar(message = message, view = coordinatorFavoriteMovies)
    }

    override fun showLoadingIndicator(isLoading: Boolean, isError: Boolean, hasData: Boolean) {
        progressBar.isVisible = isLoading
        tvEmptyRemark.isVisible = !isLoading && !isError && !hasData
    }

    override fun showError(message: String, isLoading: Boolean) {
        constraintErrorState.isVisible = message.isNotEmpty()
        tvErrorRemark.text = message
        btnRetry.setOnClickListener {
            presenter.initialize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
