package com.dedechandran.onboardingproject.presentation.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseActivity
import com.dedechandran.onboardingproject.presentation.favorite.FavoriteMovieFragment
import com.dedechandran.onboardingproject.presentation.popular.PopularMovieFragment
import com.dedechandran.onboardingproject.presentation.toprated.TopRatedMovieFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import java.lang.IllegalArgumentException

class HomeActivity : BaseActivity() {

    private lateinit var bottomNavigation: BottomNavigationView
    private lateinit var constraintHome: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initView()
        initActivity()
    }

    private fun initView() {
        bottomNavigation = findViewById(R.id.bottom_navigation)
        constraintHome = findViewById(R.id.constraint_home)
    }

    private fun initActivity() {
        bottomNavigation.setOnItemSelectedListener { menu ->
            when (menu.itemId) {
                R.id.menu_home_popular -> {
                    addFragment(
                        fragment = PopularMovieFragment(),
                        tag = PopularMovieFragment::class.simpleName
                    )
                    true
                }
                R.id.menu_home_top_rated -> {
                    addFragment(
                        fragment = TopRatedMovieFragment(),
                        tag = TopRatedMovieFragment::class.simpleName
                    )
                    true
                }
                R.id.menu_home_favorite -> {
                    addFragment(
                        fragment = FavoriteMovieFragment(),
                        tag = FavoriteMovieFragment::class.simpleName
                    )
                    true
                }
                else -> throw IllegalArgumentException("Invalid menu id")
            }
        }
    }

    private fun addFragment(fragment: Fragment, tag: String?) {
        val transaction = supportFragmentManager.beginTransaction()
        val f = supportFragmentManager.findFragmentByTag(tag)
        if (f != null) {
            transaction.replace(R.id.fragment_container, f, tag)
                .setReorderingAllowed(true)
                .commit()
        } else {
            transaction.replace(R.id.fragment_container, fragment, tag)
                .setReorderingAllowed(true)
                .commit()
        }
    }

    companion object {
        fun getIntent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }
    }
}
