package com.dedechandran.onboardingproject.presentation.popular

import com.dedechandran.onboardingproject.base.BaseView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem

interface PopularMovieView : BaseView {
    fun showPopularMovieItems(items: List<MovieRecyclerViewItem>)
    fun showLoadMoreIndicator(isLoading: Boolean)
    fun setPagination(page: Int)
    fun setIsLastPage(isLastPage: Boolean)
    fun navigateToMovieDetails(id: Int)
}
