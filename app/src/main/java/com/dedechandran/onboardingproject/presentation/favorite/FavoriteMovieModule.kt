package com.dedechandran.onboardingproject.presentation.favorite

import dagger.Binds
import dagger.Module

@Module
abstract class FavoriteMovieModule {
    @Binds
    abstract fun bindFavoriteView(favoriteMovieFragment: FavoriteMovieFragment): FavoriteMovieView
}
