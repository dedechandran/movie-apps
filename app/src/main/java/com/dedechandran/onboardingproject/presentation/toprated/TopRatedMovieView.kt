package com.dedechandran.onboardingproject.presentation.toprated

import com.dedechandran.onboardingproject.base.BaseView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem

interface TopRatedMovieView : BaseView {
    fun showTopRatedMovieItems(items: List<MovieRecyclerViewItem>)
    fun showLoadMoreIndicator(isLoading: Boolean)
    fun setPagination(page: Int)
    fun setIsLastPage(isLastPage: Boolean)
    fun navigateToMovieDetails(id: Int)
}
