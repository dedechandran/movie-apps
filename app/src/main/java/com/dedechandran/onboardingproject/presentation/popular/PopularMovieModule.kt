package com.dedechandran.onboardingproject.presentation.popular

import dagger.Binds
import dagger.Module

@Module
abstract class PopularMovieModule {

    @Binds
    abstract fun providePopularMovieView(popularMovieFragment: PopularMovieFragment): PopularMovieView
}
