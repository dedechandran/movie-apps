package com.dedechandran.onboardingproject.presentation.favorite

import com.dedechandran.domain.base.BaseObserver
import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.usecase.GetFavoriteMoviesUseCase
import com.dedechandran.onboardingproject.base.BasePresenter
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import com.dedechandran.onboardingproject.util.displayAsHyphenIfEmpty
import com.dedechandran.onboardingproject.util.formatDate
import javax.inject.Inject

class FavoriteMoviePresenter @Inject constructor(
    private val getFavoriteMoviesUseCase: GetFavoriteMoviesUseCase,
    private val errorMessageFactory: ErrorMessageFactory,
    private val view: FavoriteMovieView
) : BasePresenter {

    private var page = INITIAL_PAGE
    private val favoriteMovies = mutableListOf<FavoriteMovie.FavoriteMovieItem>()

    fun initialize() {
        loadFavoriteMovies()
    }

    fun onLoadMoreFavoriteMovies(page: Int) {
        loadFavoriteMovies(page = page, isLoadMore = true)
    }

    private fun loadFavoriteMovies(page: Int = INITIAL_PAGE, isLoadMore: Boolean = false) {
        val favoriteMovieObserver = object : BaseObserver<FavoriteMovie>() {
            override fun onStart() {
                onStartLoadingFavoriteMovie(isLoadMore = isLoadMore)
            }

            override fun onNext(t: FavoriteMovie) {
                onSuccessLoadingFavoriteMovie(t)
            }

            override fun onError(e: Throwable?) {
                onErrorLoadingFavoriteMovie(isLoadMore = isLoadMore, t = e)
            }

            override fun onComplete() {
                onCompleteLoadingFavoriteMovie(isLoadMore = isLoadMore)
            }
        }
        this.page = page
        val params = GetFavoriteMoviesUseCase.Params(
            page = page
        )
        getFavoriteMoviesUseCase.execute(observer = favoriteMovieObserver, params = params)
    }

    private fun onStartLoadingFavoriteMovie(isLoadMore: Boolean) {
        val errorMessage = EMPTY_STRING
        val isLoading = true
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = EMPTY_STRING, isLoading = isLoading)
        }
    }

    private fun onSuccessLoadingFavoriteMovie(result: FavoriteMovie) {
        favoriteMovies += result.items
        val displayedItems = favoriteMovies.map {
            MovieRecyclerViewItem.Movie(
                id = it.id,
                title = it.title.displayAsHyphenIfEmpty(),
                overview = it.overview.displayAsHyphenIfEmpty(),
                releaseDate = if (it.releaseDate.isEmpty()) {
                    it.releaseDate.displayAsHyphenIfEmpty()
                } else {
                    it.releaseDate.formatDate()
                },
                imgUrl = it.imgUrl
            )
        }
        view.showFavoriteMovieItems(displayedItems)
        view.setPagination(result.page)
        view.setIsLastPage(result.isLastPage)
    }

    private fun onErrorLoadingFavoriteMovie(isLoadMore: Boolean, t: Throwable?) {
        val errorMessage = errorMessageFactory.create(t)
        val isLoading = false
        if (isLoadMore) {
            view.setPagination(page.dec())
            view.showLoadMoreIndicator(isLoading = isLoading)
            view.showSnackBar(errorMessage)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = errorMessage, isLoading = isLoading)
        }
    }

    private fun onCompleteLoadingFavoriteMovie(isLoadMore: Boolean) {
        val isLoading = false
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, hasData = favoriteMovies.isNotEmpty())
        }
    }

    fun onItemClicked(id: Int) {
        view.navigateToMovieDetails(id = id)
    }

    fun clearData() {
        favoriteMovies.clear()
    }

    override fun onDestroy() {
        getFavoriteMoviesUseCase.dispose()
    }

    companion object {
        private const val EMPTY_STRING = ""
        private const val INITIAL_PAGE = 1
    }
}
