package com.dedechandran.onboardingproject.presentation.toprated

import dagger.Binds
import dagger.Module

@Module
abstract class TopRatedMovieModule {
    @Binds
    abstract fun bindTopRatedMovieView(topRatedMovieFragment: TopRatedMovieFragment): TopRatedMovieView
}
