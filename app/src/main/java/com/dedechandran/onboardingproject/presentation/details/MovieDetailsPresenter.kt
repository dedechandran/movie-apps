package com.dedechandran.onboardingproject.presentation.details

import com.dedechandran.domain.base.BaseObserver
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.usecase.DeleteFavoriteMovieUseCase
import com.dedechandran.domain.usecase.GetMovieDetailsUseCase
import com.dedechandran.domain.usecase.InsertFavoriteMovieUseCase
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BasePresenter
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import com.dedechandran.onboardingproject.util.displayAsHyphenIfEmpty
import com.dedechandran.onboardingproject.util.formatDate
import com.dedechandran.onboardingproject.util.manager.ResourceManager
import javax.inject.Inject

class MovieDetailsPresenter @Inject constructor(
    private val getMovieDetailsUseCase: GetMovieDetailsUseCase,
    private val insertFavoriteMovieUseCase: InsertFavoriteMovieUseCase,
    private val deleteFavoriteMovieUseCase: DeleteFavoriteMovieUseCase,
    private val errorMessageFactory: ErrorMessageFactory,
    private val resourceManager: ResourceManager,
    private val view: MovieDetailsView
) : BasePresenter {

    private var movieDetails: MovieDetails? = null

    fun initialize(id: Int) {
        val movieDetailsObserver = object : BaseObserver<MovieDetails>() {
            override fun onStart() {
                onStartLoadingMovieDetails()
            }

            override fun onNext(t: MovieDetails) {
                onSuccessLoadingMovieDetails(details = t)
            }

            override fun onError(e: Throwable?) {
                onErrorLoadingMovieDetails(e)
            }

            override fun onComplete() {
                onCompleteLoadingMovieDetails()
            }
        }
        val params = GetMovieDetailsUseCase.Params(
            id = id
        )
        getMovieDetailsUseCase.execute(observer = movieDetailsObserver, params = params)
    }

    private fun onStartLoadingMovieDetails() {
        val errorMessage = EMPTY_STRING
        val isLoading = true
        view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
        view.showError(message = errorMessage, isLoading = isLoading)
    }

    private fun onSuccessLoadingMovieDetails(details: MovieDetails) {
        movieDetails = details
        val displayedMovieDetails = details.copy(
            releaseDate = if (details.releaseDate.isEmpty()) {
                details.releaseDate.displayAsHyphenIfEmpty()
            } else {
                details.releaseDate.formatDate()
            },
            title = details.title.displayAsHyphenIfEmpty(),
            overview = details.overview.displayAsHyphenIfEmpty(),
        )
        view.showMovieDetails(displayedMovieDetails)
    }

    private fun onCompleteLoadingMovieDetails() {
        view.showLoadingIndicator(isLoading = false, isError = false)
    }

    private fun onErrorLoadingMovieDetails(t: Throwable?) {
        val errorMessage = errorMessageFactory.create(t)
        val isLoading = false
        view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
        view.showError(errorMessage, isLoading = isLoading)
    }

    fun onTrailerItemClicked(key: String) {
        view.navigateToYoutubeApp(key = key)
    }

    fun onFavoriteIconClicked(isFavorite: Boolean) {
        if (isFavorite) {
            insertFavoriteMovie(isFavorite)
        } else {
            deleteFavoriteMovie(isFavorite)
        }
    }

    private fun insertFavoriteMovie(isFavorite: Boolean) {
        val insertFavoriteMovieObserver = object : BaseObserver<Unit>() {
            override fun onError(e: Throwable?) {
                val errorMessage = errorMessageFactory.create(e)
                view.showSnackBar(errorMessage)
            }

            override fun onComplete() {
                val message = resourceManager.getString(R.string.details_successfully_to_add_favorite_movie_snackbar_message)
                view.showSnackBar(message)
                view.updateFavoriteIcon(isFavorite)
            }
        }
        val params = InsertFavoriteMovieUseCase.Params(
            id = movieDetails?.id ?: DEFAULT_ID,
            title = movieDetails?.title.orEmpty(),
            overview = movieDetails?.overview.orEmpty(),
            imgUrl = movieDetails?.imgUrl.orEmpty(),
            releaseDate = movieDetails?.releaseDate.orEmpty()
        )
        insertFavoriteMovieUseCase.execute(observer = insertFavoriteMovieObserver, params = params)
    }

    private fun deleteFavoriteMovie(isFavorite: Boolean) {
        val deleteFavoriteMovieObserver = object : BaseObserver<Unit>() {
            override fun onError(e: Throwable?) {
                val errorMessage = errorMessageFactory.create(e)
                view.showSnackBar(errorMessage)
            }

            override fun onComplete() {
                val message = resourceManager.getString(R.string.details_successfully_to_remove_favorite_movie_snackbar_message)
                view.showSnackBar(message)
                view.updateFavoriteIcon(isFavorite)
            }
        }
        val params = DeleteFavoriteMovieUseCase.Params(
            id = movieDetails?.id ?: DEFAULT_ID,
            title = movieDetails?.title.orEmpty(),
            overview = movieDetails?.overview.orEmpty(),
            imgUrl = movieDetails?.imgUrl.orEmpty(),
            releaseDate = movieDetails?.releaseDate.orEmpty()
        )
        deleteFavoriteMovieUseCase.execute(observer = deleteFavoriteMovieObserver, params = params)
    }

    override fun onDestroy() {
        getMovieDetailsUseCase.dispose()
    }

    companion object {
        private const val EMPTY_STRING = ""
        private const val DEFAULT_ID = -1
    }
}
