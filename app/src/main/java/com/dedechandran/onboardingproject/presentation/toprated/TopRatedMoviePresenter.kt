package com.dedechandran.onboardingproject.presentation.toprated

import com.dedechandran.domain.base.BaseObserver
import com.dedechandran.domain.model.TopRatedMovie
import com.dedechandran.domain.usecase.GetTopRatedMoviesUseCase
import com.dedechandran.onboardingproject.base.BasePresenter
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import com.dedechandran.onboardingproject.util.displayAsHyphenIfEmpty
import com.dedechandran.onboardingproject.util.formatDate
import javax.inject.Inject

class TopRatedMoviePresenter @Inject constructor(
    private val getTopRatedMoviesUseCase: GetTopRatedMoviesUseCase,
    private val errorMessageFactory: ErrorMessageFactory,
    private val view: TopRatedMovieView
) : BasePresenter {

    private var page = INITIAL_PAGE
    private var topRatedMovies = mutableListOf<TopRatedMovie.TopRatedMovieItem>()

    fun initialize() {
        loadTopRatedMovies()
    }

    fun onLoadMoreTopRatedMovies(page: Int) {
        loadTopRatedMovies(page = page, isLoadMore = true)
    }

    fun onItemClicked(id: Int) {
        view.navigateToMovieDetails(id = id)
    }

    private fun loadTopRatedMovies(page: Int = INITIAL_PAGE, isLoadMore: Boolean = false) {
        val topRatedMovieObserver = object : BaseObserver<TopRatedMovie>() {

            override fun onStart() {
                onStartLoadingTopRatedMovies(isLoadMore = isLoadMore)
            }

            override fun onNext(t: TopRatedMovie) {
                onSuccessLoadingTopRatedMovies(result = t)
            }

            override fun onError(e: Throwable?) {
                onErrorLoadingTopRatedMovies(isLoadMore = isLoadMore, t = e)
            }

            override fun onComplete() {
                onCompleteLoadingTopRatedMovies(isLoadMore = isLoadMore)
            }
        }
        this.page = page
        val params = GetTopRatedMoviesUseCase.Params(
            page = this.page
        )
        getTopRatedMoviesUseCase.execute(observer = topRatedMovieObserver, params = params)
    }

    private fun onSuccessLoadingTopRatedMovies(result: TopRatedMovie) {
        topRatedMovies += result.items
        val displayedItems = topRatedMovies.map {
            MovieRecyclerViewItem.Movie(
                id = it.id,
                title = it.title.displayAsHyphenIfEmpty(),
                overview = it.overview.displayAsHyphenIfEmpty(),
                releaseDate = if (it.releaseDate.isEmpty()) {
                    it.releaseDate.displayAsHyphenIfEmpty()
                } else {
                    it.releaseDate.formatDate()
                },
                imgUrl = it.imgUrl
            )
        }
        view.showTopRatedMovieItems(displayedItems)
        view.setPagination(result.page)
        view.setIsLastPage(result.isLastPage)
    }

    private fun onStartLoadingTopRatedMovies(isLoadMore: Boolean) {
        val errorMessage = EMPTY_STRING
        val isLoading = true
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = EMPTY_STRING, isLoading = isLoading)
        }
    }

    private fun onCompleteLoadingTopRatedMovies(isLoadMore: Boolean) {
        val isLoading = false
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading)
        }
    }

    private fun onErrorLoadingTopRatedMovies(isLoadMore: Boolean, t: Throwable?) {
        val errorMessage = errorMessageFactory.create(t)
        val isLoading = false
        if (isLoadMore) {
            view.setPagination(page.dec())
            view.showLoadMoreIndicator(isLoading = isLoading)
            view.showSnackBar(errorMessage)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = errorMessage, isLoading = isLoading)
        }
    }

    override fun onDestroy() {
        getTopRatedMoviesUseCase.dispose()
    }

    companion object {
        private const val INITIAL_PAGE = 1
        private const val EMPTY_STRING = ""
    }
}
