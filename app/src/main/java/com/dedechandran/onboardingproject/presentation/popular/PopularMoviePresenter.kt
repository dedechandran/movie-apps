package com.dedechandran.onboardingproject.presentation.popular

import com.dedechandran.domain.base.BaseObserver
import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.usecase.GetPopularMoviesUseCase
import com.dedechandran.onboardingproject.base.BasePresenter
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.ErrorMessageFactory
import com.dedechandran.onboardingproject.util.displayAsHyphenIfEmpty
import com.dedechandran.onboardingproject.util.formatDate
import javax.inject.Inject

class PopularMoviePresenter @Inject constructor(
    private val getPopularMoviesUseCase: GetPopularMoviesUseCase,
    private val errorMessageFactory: ErrorMessageFactory,
    private val view: PopularMovieView
) : BasePresenter {

    private var page = INITIAL_PAGE
    private var popularMovies = mutableListOf<PopularMovie.PopularMovieItem>()

    fun initialize() {
        loadPopularMovies()
    }

    fun onLoadMorePopularMovies(page: Int) {
        loadPopularMovies(page = page, isLoadMore = true)
    }

    fun onItemClicked(id: Int) {
        view.navigateToMovieDetails(id = id)
    }

    private fun loadPopularMovies(page: Int = INITIAL_PAGE, isLoadMore: Boolean = false) {
        val popularMovieObserver = object : BaseObserver<PopularMovie>() {

            override fun onStart() {
                onStartLoadingPopularMovies(isLoadMore = isLoadMore)
            }

            override fun onNext(t: PopularMovie) {
                onSuccessLoadingPopularMovies(result = t)
            }

            override fun onError(e: Throwable?) {
                onErrorLoadingPopularMovies(isLoadMore = isLoadMore, t = e)
            }

            override fun onComplete() {
                onCompleteLoadingPopularMovies(isLoadMore = isLoadMore)
            }
        }
        this.page = page
        val params = GetPopularMoviesUseCase.Params(
            page = this.page
        )
        getPopularMoviesUseCase.execute(observer = popularMovieObserver, params = params)
    }

    private fun onSuccessLoadingPopularMovies(result: PopularMovie) {
        popularMovies += result.items
        val displayedItems = popularMovies.map {
            MovieRecyclerViewItem.Movie(
                id = it.id,
                title = it.title.displayAsHyphenIfEmpty(),
                overview = it.overview.displayAsHyphenIfEmpty(),
                releaseDate = if (it.releaseDate.isEmpty()) {
                    it.releaseDate.displayAsHyphenIfEmpty()
                } else {
                    it.releaseDate.formatDate()
                },
                imgUrl = it.imgUrl
            )
        }
        view.showPopularMovieItems(displayedItems)
        view.setPagination(result.page)
        view.setIsLastPage(result.isLastPage)
    }

    private fun onStartLoadingPopularMovies(isLoadMore: Boolean) {
        val errorMessage = EMPTY_STRING
        val isLoading = true
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = EMPTY_STRING, isLoading = isLoading)
        }
    }

    private fun onCompleteLoadingPopularMovies(isLoadMore: Boolean) {
        val isLoading = false
        if (isLoadMore) {
            view.showLoadMoreIndicator(isLoading = isLoading)
        } else {
            view.showLoadingIndicator(isLoading = isLoading)
        }
    }

    private fun onErrorLoadingPopularMovies(isLoadMore: Boolean, t: Throwable?) {
        val errorMessage = errorMessageFactory.create(t)
        val isLoading = false
        if (isLoadMore) {
            view.setPagination(page.dec())
            view.showLoadMoreIndicator(isLoading = isLoading)
            view.showSnackBar(errorMessage)
        } else {
            view.showLoadingIndicator(isLoading = isLoading, isError = errorMessage.isNotEmpty())
            view.showError(message = errorMessage, isLoading = isLoading)
        }
    }

    override fun onDestroy() {
        getPopularMoviesUseCase.dispose()
    }

    companion object {
        private const val INITIAL_PAGE = 1
        private const val EMPTY_STRING = ""
    }
}
