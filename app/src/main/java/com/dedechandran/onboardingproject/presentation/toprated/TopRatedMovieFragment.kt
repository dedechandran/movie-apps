package com.dedechandran.onboardingproject.presentation.toprated

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.isVisible
import com.dedechandran.onboardingproject.R
import com.dedechandran.onboardingproject.base.BaseFragment
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerView
import com.dedechandran.onboardingproject.base.ui.movie.MovieRecyclerViewItem
import com.dedechandran.onboardingproject.util.makeSnackBar
import com.google.android.material.button.MaterialButton
import javax.inject.Inject

class TopRatedMovieFragment : BaseFragment(), TopRatedMovieView {

    @Inject
    lateinit var presenter: TopRatedMoviePresenter

    private lateinit var toolbar: Toolbar
    private lateinit var rvTopRatedMovies: MovieRecyclerView
    private lateinit var progressBar: ProgressBar
    private lateinit var constraintErrorState: ConstraintLayout
    private lateinit var tvErrorRemark: TextView
    private lateinit var btnRetry: MaterialButton
    private lateinit var coordinatorTopRatedMovies: CoordinatorLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_top_rated_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view)
        initFragment()
    }

    private fun initView(view: View) {
        with(view) {
            toolbar = findViewById(R.id.toolbar)
            rvTopRatedMovies = findViewById(R.id.rv_top_rated_movies)
            progressBar = findViewById(R.id.progress_bar)
            constraintErrorState = findViewById(R.id.constraint_error_state)
            tvErrorRemark = constraintErrorState.findViewById(R.id.tv_error_remark)
            btnRetry = constraintErrorState.findViewById(R.id.btn_retry)
            coordinatorTopRatedMovies = findViewById(R.id.coordinator_top_rated_movies)

            (requireActivity() as AppCompatActivity).setSupportActionBar(toolbar)

            rvTopRatedMovies.apply {
                setOnLoadMoreListener {
                    presenter.onLoadMoreTopRatedMovies(it.inc())
                }
                setOnItemClickListener {
                    presenter.onItemClicked(it)
                }
                setHasFixedSize(true)
            }
        }
    }

    private fun initFragment() {
        presenter.initialize()
    }

    override fun showTopRatedMovieItems(items: List<MovieRecyclerViewItem>) {
        rvTopRatedMovies.setItems(items = items)
    }

    override fun showLoadMoreIndicator(isLoading: Boolean) {
        rvTopRatedMovies.setIsLoading(isLoading)
    }

    override fun setPagination(page: Int) {
        rvTopRatedMovies.setPagination(page = page)
    }

    override fun setIsLastPage(isLastPage: Boolean) {
        rvTopRatedMovies.setIsLastPage(isLastPage = isLastPage)
    }

    override fun navigateToMovieDetails(id: Int) {
        navigator.navigateToMovieDetails(context = requireContext(), movieId = id)
    }

    override fun showSnackBar(message: String) {
        requireContext().makeSnackBar(message = message, view = coordinatorTopRatedMovies)
    }

    override fun showLoadingIndicator(isLoading: Boolean, isError: Boolean, hasData: Boolean) {
        progressBar.isVisible = isLoading
    }

    override fun showError(message: String, isLoading: Boolean) {
        constraintErrorState.isVisible = message.isNotEmpty()
        tvErrorRemark.text = message
        btnRetry.setOnClickListener {
            presenter.initialize()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}
