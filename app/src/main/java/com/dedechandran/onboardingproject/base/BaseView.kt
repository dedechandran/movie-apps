package com.dedechandran.onboardingproject.base

interface BaseView {
    fun showSnackBar(message: String)
    fun showLoadingIndicator(isLoading: Boolean, isError: Boolean = false, hasData: Boolean = false)
    fun showError(message: String, isLoading: Boolean = false)
}
