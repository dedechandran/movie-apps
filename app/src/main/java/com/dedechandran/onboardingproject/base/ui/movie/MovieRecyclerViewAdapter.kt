package com.dedechandran.onboardingproject.base.ui.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dedechandran.onboardingproject.R
import java.lang.IllegalArgumentException

class MovieRecyclerViewAdapter : RecyclerView.Adapter<MovieRecyclerViewHolder>() {

    private val items = mutableListOf<MovieRecyclerViewItem>()
    private var onItemClickListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieRecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            MOVIE_ITEM -> MovieRecyclerViewHolder.MovieViewHolder(
                view = layoutInflater.inflate(R.layout.view_movie_item, parent, false),
                onItemClickListener = onItemClickListener
            )
            LOADING_ITEM -> MovieRecyclerViewHolder.LoadingViewHolder(
                view = layoutInflater.inflate(R.layout.view_movie_loading_item, parent, false)
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: MovieRecyclerViewHolder, position: Int) {
        when (holder) {
            is MovieRecyclerViewHolder.MovieViewHolder -> holder.bind(items[position] as MovieRecyclerViewItem.Movie)
            is MovieRecyclerViewHolder.LoadingViewHolder -> holder.bind(items[position] as MovieRecyclerViewItem.Loading)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position]) {
            is MovieRecyclerViewItem.Movie -> MOVIE_ITEM
            is MovieRecyclerViewItem.Loading -> LOADING_ITEM
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnItemClickListener(onItemClickListener: (Int) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }

    fun setItems(items: List<MovieRecyclerViewItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun showLoadingItem() {
        this.items.add(MovieRecyclerViewItem.Loading)
        notifyItemInserted(this.items.lastIndex)
    }

    fun hideLoadingItem() {
        this.items.remove(MovieRecyclerViewItem.Loading)
        notifyItemRemoved(this.items.lastIndex)
    }

    companion object {
        private const val MOVIE_ITEM = 1
        private const val LOADING_ITEM = 2
    }
}
