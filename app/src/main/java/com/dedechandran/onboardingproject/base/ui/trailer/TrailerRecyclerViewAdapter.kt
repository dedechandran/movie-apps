package com.dedechandran.onboardingproject.base.ui.trailer

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dedechandran.onboardingproject.R

class TrailerRecyclerViewAdapter : RecyclerView.Adapter<TrailerRecyclerViewHolder>() {

    private val items = mutableListOf<TrailerRecyclerViewItem>()
    private var onItemClickListener: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrailerRecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return TrailerRecyclerViewHolder(
            onItemClickListener = onItemClickListener,
            view = layoutInflater.inflate(R.layout.view_trailer_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: TrailerRecyclerViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(items: List<TrailerRecyclerViewItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(onItemClickListener: (String) -> Unit) {
        this.onItemClickListener = onItemClickListener
    }
}
