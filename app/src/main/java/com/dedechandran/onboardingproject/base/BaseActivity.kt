package com.dedechandran.onboardingproject.base

import com.dedechandran.onboardingproject.navigation.Navigator
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

open class BaseActivity : DaggerAppCompatActivity() {
    @Inject lateinit var navigator: Navigator
}
