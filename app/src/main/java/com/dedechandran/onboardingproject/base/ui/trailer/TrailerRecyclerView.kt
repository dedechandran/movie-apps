package com.dedechandran.onboardingproject.base.ui.trailer

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class TrailerRecyclerView @JvmOverloads constructor(
    context: Context,
    attrSet: AttributeSet? = null,
    styleRes: Int = 0
) : RecyclerView(context, attrSet, styleRes) {

    private val trailerAdapter by lazy {
        TrailerRecyclerViewAdapter()
    }

    init {
        super.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
        super.setAdapter(trailerAdapter)
    }

    fun setItems(items: List<TrailerRecyclerViewItem>) {
        post {
            trailerAdapter.setItems(items)
        }
    }

    fun setOnItemClickListener(onItemClickListener: (String) -> Unit) {
        trailerAdapter.setOnItemClickListener(onItemClickListener)
    }
}
