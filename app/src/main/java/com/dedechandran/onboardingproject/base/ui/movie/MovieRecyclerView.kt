package com.dedechandran.onboardingproject.base.ui.movie

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MovieRecyclerView @JvmOverloads constructor(
    context: Context,
    attrSet: AttributeSet? = null,
    styleRes: Int = 0
) : RecyclerView(context, attrSet, styleRes) {

    private val movieAdapter: MovieRecyclerViewAdapter by lazy {
        MovieRecyclerViewAdapter()
    }

    private var movieScrollListener: MovieRecyclerViewScrollListener? = null

    init {
        super.setLayoutManager(LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false))
        super.setAdapter(movieAdapter)
    }

    fun setItems(items: List<MovieRecyclerViewItem>) {
        post {
            movieAdapter.setItems(items)
        }
    }

    fun setOnLoadMoreListener(onLoadMore: (Int) -> Unit) {
        movieScrollListener = object : MovieRecyclerViewScrollListener() {
            override fun loadMore(page: Int) {
                onLoadMore.invoke(page)
            }
        }
        movieScrollListener?.let {
            addOnScrollListener(it)
        }
    }

    fun setPagination(page: Int) {
        movieScrollListener?.page = page
    }

    fun setIsLoading(isLoading: Boolean) {
        movieScrollListener?.isLoading = isLoading
        post {
            if (isLoading) {
                movieAdapter.showLoadingItem()
            } else {
                movieAdapter.hideLoadingItem()
            }
        }
    }

    fun setIsLastPage(isLastPage: Boolean) {
        movieScrollListener?.isLastPage = isLastPage
    }

    fun setOnItemClickListener(onItemClickListener: (Int) -> Unit) {
        movieAdapter.setOnItemClickListener(onItemClickListener)
    }
}
