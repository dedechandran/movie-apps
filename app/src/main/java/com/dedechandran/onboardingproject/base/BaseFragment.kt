package com.dedechandran.onboardingproject.base

import com.dedechandran.onboardingproject.navigation.Navigator
import dagger.android.support.DaggerFragment
import javax.inject.Inject

open class BaseFragment : DaggerFragment() {
    @Inject lateinit var navigator: Navigator
}
