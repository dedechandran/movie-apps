package com.dedechandran.onboardingproject.base.ui.trailer

data class TrailerRecyclerViewItem(
    val name: String,
    val key: String
)
