package com.dedechandran.onboardingproject.base.ui.trailer

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.dedechandran.onboardingproject.R

class TrailerRecyclerViewHolder(
    private val onItemClickListener: ((String) -> Unit)? = null,
    view: View
) : RecyclerView.ViewHolder(view) {

    fun bind(item: TrailerRecyclerViewItem) {
        with(itemView) {
            val tvTrailerName = findViewById<TextView>(R.id.tv_trailer_name)
            tvTrailerName.text = item.name
            setOnClickListener {
                onItemClickListener?.invoke(item.key)
            }
        }
    }
}
