package com.dedechandran.onboardingproject.base

interface BasePresenter {
    fun onDestroy()
}
