package com.dedechandran.onboardingproject.base.ui.movie

sealed class MovieRecyclerViewItem {
    data class Movie(
        val id: Int,
        val title: String,
        val overview: String,
        val releaseDate: String,
        val imgUrl: String
    ) : MovieRecyclerViewItem()

    object Loading : MovieRecyclerViewItem()
}
