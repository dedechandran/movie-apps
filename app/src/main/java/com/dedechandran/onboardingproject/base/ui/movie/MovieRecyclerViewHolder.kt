package com.dedechandran.onboardingproject.base.ui.movie

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.dedechandran.onboardingproject.R

sealed class MovieRecyclerViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    class MovieViewHolder(
        private val onItemClickListener: ((Int) -> Unit)? = null,
        view: View
    ) :
        MovieRecyclerViewHolder(view) {

        fun bind(item: MovieRecyclerViewItem.Movie) {
            val tvMovieTitle = itemView.findViewById<TextView>(R.id.tv_movie_title)
            val tvMovieOverview = itemView.findViewById<TextView>(R.id.tv_movie_overview)
            val tvMovieReleaseDate = itemView.findViewById<TextView>(R.id.tv_movie_release_date)
            val ivMovieImage = itemView.findViewById<ImageView>(R.id.iv_movie_image)

            tvMovieTitle.text = item.title
            tvMovieOverview.text = item.overview
            tvMovieReleaseDate.text = item.releaseDate
            ivMovieImage.load(item.imgUrl)
            itemView.setOnClickListener {
                onItemClickListener?.invoke(item.id)
            }
        }
    }

    class LoadingViewHolder(view: View) : MovieRecyclerViewHolder(view) {
        fun bind(item: MovieRecyclerViewItem.Loading) {
            // Do nothing
        }
    }
}
