package com.dedechandran.onboardingproject.base.ui.movie

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class MovieRecyclerViewScrollListener : RecyclerView.OnScrollListener() {

    var page = 1
    var isLoading = false
    var isLastPage = false
    private var loadMoreThreshold = 2

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy <= 0) return
        if (isLastPage) return
        if (isLoading) return
        if (recyclerView.layoutManager == null) return
        val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
        val totalItems = layoutManager?.itemCount ?: 0
        val visibleItems = layoutManager?.childCount ?: 0
        val scrolledOutItems = layoutManager?.findFirstVisibleItemPosition() ?: 0

        if (visibleItems + scrolledOutItems == totalItems - loadMoreThreshold) {
            isLoading = true
            loadMore(page = page)
        }
    }

    abstract fun loadMore(page: Int)
}
