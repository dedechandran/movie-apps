package com.dedechandran.onboardingproject.di

import com.dedechandran.onboardingproject.presentation.favorite.FavoriteMovieFragment
import com.dedechandran.onboardingproject.presentation.favorite.FavoriteMovieModule
import com.dedechandran.onboardingproject.presentation.popular.PopularMovieFragment
import com.dedechandran.onboardingproject.presentation.popular.PopularMovieModule
import com.dedechandran.onboardingproject.presentation.toprated.TopRatedMovieFragment
import com.dedechandran.onboardingproject.presentation.toprated.TopRatedMovieModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeFragmentModule {
    @ContributesAndroidInjector(modules = [PopularMovieModule::class])
    abstract fun bindPopularMovieFragment(): PopularMovieFragment

    @ContributesAndroidInjector(modules = [TopRatedMovieModule::class])
    abstract fun bindTopRatedMovieFragment(): TopRatedMovieFragment

    @ContributesAndroidInjector(modules = [FavoriteMovieModule::class])
    abstract fun bindFavoriteMovieFragment(): FavoriteMovieFragment
}
