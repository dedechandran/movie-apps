package com.dedechandran.onboardingproject.di

import android.content.Context
import com.dedechandran.domain.base.BaseSchedulerProvider
import com.dedechandran.domain.scheduler.SchedulerProvider
import com.dedechandran.onboardingproject.MainApp
import com.dedechandran.onboardingproject.util.manager.ResourceManager
import com.dedechandran.onboardingproject.util.manager.ResourceManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModule {

    @Binds
    @Singleton
    abstract fun bindAppContext(mainApp: MainApp): Context

    @Binds
    @Singleton
    abstract fun bindAppScheduler(appSchedulerProvider: SchedulerProvider): BaseSchedulerProvider

    @Binds
    @Singleton
    abstract fun bindResourceManager(resourceManagerImpl: ResourceManagerImpl): ResourceManager
}
