package com.dedechandran.onboardingproject.di

import com.dedechandran.data.di.DataSourceModule
import com.dedechandran.data.di.DatabaseModule
import com.dedechandran.data.di.NetworkModule
import com.dedechandran.data.di.RepositoryModule
import com.dedechandran.data.di.ServiceModule
import com.dedechandran.onboardingproject.MainApp
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        ActivityModule::class,
        ServiceModule::class,
        DataSourceModule::class,
        RepositoryModule::class,
        DatabaseModule::class
    ]
)
interface AppComponent : AndroidInjector<MainApp> {
    @Component.Factory
    abstract class Builder : AndroidInjector.Factory<MainApp>
}
