package com.dedechandran.onboardingproject.di

import com.dedechandran.onboardingproject.presentation.details.MovieDetailsActivity
import com.dedechandran.onboardingproject.presentation.details.MovieDetailsModule
import com.dedechandran.onboardingproject.presentation.home.HomeActivity
import com.dedechandran.onboardingproject.presentation.splash.SplashActivity
import com.dedechandran.onboardingproject.presentation.splash.SplashModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    abstract fun bindMainActivity(): HomeActivity

    @ContributesAndroidInjector(modules = [MovieDetailsModule::class])
    abstract fun bindMovieDetailsActivity(): MovieDetailsActivity

    @ContributesAndroidInjector(modules = [SplashModule::class])
    abstract fun bindSplashActivity(): SplashActivity
}
