package com.dedechandran.onboardingproject.navigation

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.dedechandran.onboardingproject.presentation.details.MovieDetailsActivity
import com.dedechandran.onboardingproject.presentation.home.HomeActivity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigator @Inject constructor() {

    fun navigateToMovieDetails(context: Context, movieId: Int) {
        val intent = MovieDetailsActivity.getIntent(context = context, movieId = movieId)
        context.startActivity(intent)
    }

    fun navigateToYoutubeApp(context: Context, key: String) {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$key"))
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$key"))
        try {
            context.startActivity(appIntent)
        } catch (activityException: ActivityNotFoundException) {
            context.startActivity(browserIntent)
        }
    }

    fun navigateToHomeScreen(context: Context) {
        val intent = HomeActivity.getIntent(context = context).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        }
        context.startActivity(intent)
    }
}
