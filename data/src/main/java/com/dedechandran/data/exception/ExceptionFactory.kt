package com.dedechandran.data.exception

import com.dedechandran.data.datasource.DataSourceType
import com.dedechandran.domain.model.error.Error
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection
import java.sql.SQLException
import javax.inject.Inject

class ExceptionFactory @Inject constructor() {

    fun create(type: DataSourceType, t: Throwable): Error {
        return when (type) {
            DataSourceType.REMOTE -> createRemoteException(t)
            DataSourceType.LOCAL -> createLocalException(t)
        }
    }

    private fun createRemoteException(t: Throwable): Error {
        return when (t) {
            is HttpException -> {
                val code = t.code()
                when {
                    code == HttpURLConnection.HTTP_UNAUTHORIZED -> Error.InvalidApiKeyException
                    code == HttpURLConnection.HTTP_NOT_FOUND -> Error.ResourceNotFoundException
                    code >= HttpURLConnection.HTTP_INTERNAL_ERROR -> Error.InternalServerErrorException
                    else -> Error.UnknownErrorException
                }
            }
            is IOException -> {
                Error.NoNetworkException
            }
            else -> {
                Error.UnknownErrorException
            }
        }
    }

    private fun createLocalException(t: Throwable): Error {
        return when (t) {
            is IOException, is SQLException -> {
                Error.DatabaseException
            }
            else -> {
                Error.UnknownErrorException
            }
        }
    }
}
