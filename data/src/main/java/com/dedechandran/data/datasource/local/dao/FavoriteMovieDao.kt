package com.dedechandran.data.datasource.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe

@Dao
interface FavoriteMovieDao {
    @Query("SELECT * FROM favorite_movie ORDER BY movie_title ASC LIMIT :limit OFFSET :offset ")
    fun getFavoriteMovies(limit: Int, offset: Int): Maybe<List<FavoriteMovieEntity>>

    @Insert
    fun insertFavoriteMovie(movie: FavoriteMovieEntity): Completable

    @Delete
    fun deleteFavoriteMovie(movie: FavoriteMovieEntity): Completable

    @Query("SELECT COUNT(id) FROM favorite_movie")
    fun getCount(): Maybe<Int>

    @Query("SELECT * FROM favorite_movie WHERE id = :id")
    fun getFavoriteMovieById(id: Int): Maybe<List<FavoriteMovieEntity>>
}
