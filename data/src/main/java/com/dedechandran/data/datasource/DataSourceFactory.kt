package com.dedechandran.data.datasource

import com.dedechandran.data.datasource.local.LocalDataSource
import com.dedechandran.data.datasource.remote.RemoteDataSource
import javax.inject.Inject

class DataSourceFactory @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource
) {

    fun create(type: DataSourceType): DataSource {
        return when (type) {
            DataSourceType.REMOTE -> remoteDataSource
            DataSourceType.LOCAL -> localDataSource
        }
    }
}
