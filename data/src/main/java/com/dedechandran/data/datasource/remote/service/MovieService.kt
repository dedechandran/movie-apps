package com.dedechandran.data.datasource.remote.service

import com.dedechandran.data.datasource.remote.response.MovieDetailsResponse
import com.dedechandran.data.datasource.remote.response.MovieResponse
import com.dedechandran.data.datasource.remote.response.MovieVideosResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("movie/popular")
    fun getPopularMovies(@Query("page") page: Int): Observable<MovieResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovies(@Query("page") page: Int): Observable<MovieResponse>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") id: Int): Observable<MovieDetailsResponse>

    @GET("movie/{movie_id}/videos")
    fun getMovieVideos(@Path("movie_id") id: Int): Observable<MovieVideosResponse>
}
