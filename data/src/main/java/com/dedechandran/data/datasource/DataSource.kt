package com.dedechandran.data.datasource

import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import com.dedechandran.data.datasource.remote.response.MovieDetailsResponse
import com.dedechandran.data.datasource.remote.response.MovieResponse
import com.dedechandran.data.datasource.remote.response.MovieVideosResponse
import io.reactivex.rxjava3.core.Observable

interface DataSource {
    fun getPopularMovies(page: Int): Observable<MovieResponse>
    fun getTopRatedMovies(page: Int): Observable<MovieResponse>
    fun getMovieDetails(id: Int): Observable<MovieDetailsResponse>
    fun getMovieVideos(id: Int): Observable<MovieVideosResponse>
    fun getFavoriteMovies(limit: Int, offset: Int): Observable<List<FavoriteMovieEntity>>
    fun insertFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit>
    fun deleteFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit>
    fun getCountOfFavoriteMovies(): Observable<Int>
    fun getFavoriteMovieById(id: Int): Observable<List<FavoriteMovieEntity>>
}
