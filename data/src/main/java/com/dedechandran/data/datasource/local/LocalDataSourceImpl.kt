package com.dedechandran.data.datasource.local

import com.dedechandran.data.datasource.local.dao.FavoriteMovieDao
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class LocalDataSourceImpl @Inject constructor(
    private val favoriteMovieDao: FavoriteMovieDao
) : LocalDataSource {
    override fun getFavoriteMovies(limit: Int, offset: Int): Observable<List<FavoriteMovieEntity>> {
        return Observable.fromMaybe(favoriteMovieDao.getFavoriteMovies(limit = limit, offset = offset))
    }

    override fun insertFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit> {
        return Observable.fromCompletable(favoriteMovieDao.insertFavoriteMovie(favoriteMovie))
    }

    override fun deleteFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit> {
        return Observable.fromCompletable(favoriteMovieDao.deleteFavoriteMovie(favoriteMovie))
    }

    override fun getCountOfFavoriteMovies(): Observable<Int> {
        return Observable.fromMaybe(favoriteMovieDao.getCount())
    }

    override fun getFavoriteMovieById(id: Int): Observable<List<FavoriteMovieEntity>> {
        return Observable.fromMaybe(favoriteMovieDao.getFavoriteMovieById(id))
    }
}
