package com.dedechandran.data.datasource.remote.response

import com.dedechandran.domain.model.MovieDetails
import com.google.gson.annotations.SerializedName

data class MovieDetailsResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("title")
    val title: String?,
    @SerializedName("backdrop_path")
    val imgUrl: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("release_date")
    val releaseDate: String?,
    @SerializedName("runtime")
    val runtime: Int?,
    @SerializedName("genres")
    val genres: List<Genre>?,
    @SerializedName("vote_average")
    val voteAverage: Double?
) {
    data class Genre(
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        val name: String?
    )
}

fun MovieDetailsResponse.toDomain() = MovieDetails(
    id = id,
    title = title.orEmpty(),
    imgUrl = "https://image.tmdb.org/t/p/original".plus(imgUrl.orEmpty()),
    overview = overview.orEmpty(),
    releaseDate = releaseDate.orEmpty(),
    runtime = runtime ?: 0,
    genres = genres.orEmpty().map {
        it.name.orEmpty()
    },
    voteAverage = voteAverage ?: 0.0
)
