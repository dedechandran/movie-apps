package com.dedechandran.data.datasource.remote

import com.dedechandran.data.datasource.DataSource
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import io.reactivex.rxjava3.core.Observable

interface RemoteDataSource : DataSource {
    override fun getFavoriteMovies(limit: Int, offset: Int): Observable<List<FavoriteMovieEntity>> {
        throw UnsupportedOperationException()
    }

    override fun insertFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit> {
        throw UnsupportedOperationException()
    }

    override fun deleteFavoriteMovie(favoriteMovie: FavoriteMovieEntity): Observable<Unit> {
        throw UnsupportedOperationException()
    }

    override fun getCountOfFavoriteMovies(): Observable<Int> {
        throw UnsupportedOperationException()
    }

    override fun getFavoriteMovieById(id: Int): Observable<List<FavoriteMovieEntity>> {
        throw UnsupportedOperationException()
    }
}
