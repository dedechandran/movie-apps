package com.dedechandran.data.datasource.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dedechandran.data.datasource.local.dao.FavoriteMovieDao
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity

@Database(entities = [FavoriteMovieEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDao(): FavoriteMovieDao
}
