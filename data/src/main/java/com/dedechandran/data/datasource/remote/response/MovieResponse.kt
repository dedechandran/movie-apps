package com.dedechandran.data.datasource.remote.response

import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.model.TopRatedMovie
import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_results")
    val totalResult: Int,
    @SerializedName("total_pages")
    val totalPage: Int,
    @SerializedName("results")
    val results: List<Movie>
) {
    data class Movie(
        @SerializedName("id")
        val id: Int,
        @SerializedName("poster_path")
        val imageUrl: String?,
        @SerializedName("title")
        val title: String?,
        @SerializedName("overview")
        val overview: String?,
        @SerializedName("release_date")
        val releaseDate: String?
    )
}

fun MovieResponse.toPopularMovieDomain(): PopularMovie {
    return PopularMovie(
        page = page,
        isLastPage = page == totalPage,
        items = results.map {
            PopularMovie.PopularMovieItem(
                id = it.id,
                imgUrl = "https://image.tmdb.org/t/p/w500".plus(it.imageUrl.orEmpty()),
                title = it.title.orEmpty(),
                overview = it.overview.orEmpty(),
                releaseDate = it.releaseDate.orEmpty()
            )
        }
    )
}

fun MovieResponse.toTopRatedMovieDomain(): TopRatedMovie {
    return TopRatedMovie(
        page = page,
        isLastPage = page == totalPage,
        items = results.map {
            TopRatedMovie.TopRatedMovieItem(
                id = it.id,
                imgUrl = "https://image.tmdb.org/t/p/w500".plus(it.imageUrl.orEmpty()),
                title = it.title.orEmpty(),
                overview = it.overview.orEmpty(),
                releaseDate = it.releaseDate.orEmpty()
            )
        }
    )
}
