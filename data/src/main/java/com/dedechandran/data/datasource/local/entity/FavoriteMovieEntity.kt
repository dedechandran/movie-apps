package com.dedechandran.data.datasource.local.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.dedechandran.domain.model.FavoriteMovie

@Entity(tableName = "favorite_movie")
data class FavoriteMovieEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "movie_title") val movieTitle: String?,
    @ColumnInfo(name = "movie_overview") val movieOverview: String?,
    @ColumnInfo(name = "movie_release_date") val movieReleaseDate: String?,
    @ColumnInfo(name = "movie_img_url") val movieImageUrl: String?
)

fun List<FavoriteMovieEntity>.toDomain() = FavoriteMovie(
    items = map {
        FavoriteMovie.FavoriteMovieItem(
            id = it.id,
            title = it.movieTitle.orEmpty(),
            overview = it.movieOverview.orEmpty(),
            releaseDate = it.movieReleaseDate.orEmpty(),
            imgUrl = it.movieImageUrl.orEmpty()
        )
    }
)
