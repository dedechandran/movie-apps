package com.dedechandran.data.datasource.local

import com.dedechandran.data.datasource.DataSource
import com.dedechandran.data.datasource.remote.response.MovieDetailsResponse
import com.dedechandran.data.datasource.remote.response.MovieResponse
import com.dedechandran.data.datasource.remote.response.MovieVideosResponse
import io.reactivex.rxjava3.core.Observable

interface LocalDataSource : DataSource {
    override fun getPopularMovies(page: Int): Observable<MovieResponse> {
        throw UnsupportedOperationException()
    }

    override fun getTopRatedMovies(page: Int): Observable<MovieResponse> {
        throw UnsupportedOperationException()
    }

    override fun getMovieDetails(id: Int): Observable<MovieDetailsResponse> {
        throw UnsupportedOperationException()
    }

    override fun getMovieVideos(id: Int): Observable<MovieVideosResponse> {
        throw UnsupportedOperationException()
    }
}
