package com.dedechandran.data.datasource

enum class DataSourceType {
    REMOTE,
    LOCAL
}
