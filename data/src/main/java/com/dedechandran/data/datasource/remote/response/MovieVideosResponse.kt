package com.dedechandran.data.datasource.remote.response

import com.dedechandran.domain.model.MovieVideo
import com.google.gson.annotations.SerializedName

data class MovieVideosResponse(
    @SerializedName("id")
    val id: Int,
    @SerializedName("results")
    val results: List<MovieVideo>
) {
    data class MovieVideo(
        @SerializedName("name")
        val name: String,
        @SerializedName("key")
        val key: String,
        @SerializedName("site")
        val site: String,
        @SerializedName("type")
        val type: String
    )
}

fun MovieVideosResponse.toDomain() = results.map {
    MovieVideo(
        type = it.type,
        name = it.name,
        key = it.key,
        site = it.site
    )
}
