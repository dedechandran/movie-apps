package com.dedechandran.data.datasource.remote

import com.dedechandran.data.datasource.remote.response.MovieDetailsResponse
import com.dedechandran.data.datasource.remote.response.MovieResponse
import com.dedechandran.data.datasource.remote.response.MovieVideosResponse
import com.dedechandran.data.datasource.remote.service.MovieService
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(private val movieService: MovieService) :
    RemoteDataSource {
    override fun getPopularMovies(page: Int): Observable<MovieResponse> {
        return movieService.getPopularMovies(page = page)
    }

    override fun getTopRatedMovies(page: Int): Observable<MovieResponse> {
        return movieService.getTopRatedMovies(page = page)
    }

    override fun getMovieDetails(id: Int): Observable<MovieDetailsResponse> {
        return movieService.getMovieDetails(id = id)
    }

    override fun getMovieVideos(id: Int): Observable<MovieVideosResponse> {
        return movieService.getMovieVideos(id = id)
    }
}
