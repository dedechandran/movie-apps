package com.dedechandran.data

import com.dedechandran.data.datasource.DataSourceFactory
import com.dedechandran.data.datasource.DataSourceType
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import com.dedechandran.data.datasource.local.entity.toDomain
import com.dedechandran.data.datasource.remote.response.toDomain
import com.dedechandran.data.datasource.remote.response.toPopularMovieDomain
import com.dedechandran.data.datasource.remote.response.toTopRatedMovieDomain
import com.dedechandran.data.exception.ExceptionFactory
import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.model.MovieDetails
import com.dedechandran.domain.model.MovieVideo
import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.model.TopRatedMovie
import com.dedechandran.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(
    private val dataSourceFactory: DataSourceFactory,
    private val exceptionFactory: ExceptionFactory
) :
    MovieRepository {
    override fun getPopularMovies(page: Int): Observable<PopularMovie> {
        val remoteDataSource = dataSourceFactory.create(type = DataSourceType.REMOTE)
        return remoteDataSource.getPopularMovies(page = page)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.REMOTE, t = it)
                Observable.error(exception)
            }
            .map { it.toPopularMovieDomain() }
    }

    override fun getTopRatedMovies(page: Int): Observable<TopRatedMovie> {
        val remoteDataSource = dataSourceFactory.create(type = DataSourceType.REMOTE)
        return remoteDataSource.getTopRatedMovies(page = page)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.REMOTE, t = it)
                Observable.error(exception)
            }
            .map { it.toTopRatedMovieDomain() }
    }

    override fun getMovieDetails(id: Int): Observable<MovieDetails> {
        val remoteDataSource = dataSourceFactory.create(type = DataSourceType.REMOTE)
        return remoteDataSource.getMovieDetails(id = id)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.REMOTE, t = it)
                Observable.error(exception)
            }
            .map { it.toDomain() }
    }

    override fun getMovieVideos(id: Int): Observable<List<MovieVideo>> {
        val remoteDataSource = dataSourceFactory.create(type = DataSourceType.REMOTE)
        return remoteDataSource.getMovieVideos(id = id)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.REMOTE, t = it)
                Observable.error(exception)
            }
            .map { it.toDomain() }
    }

    override fun getFavoriteMovies(
        page: Int,
        limit: Int,
        offset: Int,
        isLastPage: Boolean
    ): Observable<FavoriteMovie> {
        val localDataSource = dataSourceFactory.create(type = DataSourceType.LOCAL)
        return localDataSource.getFavoriteMovies(limit = limit, offset = offset)
            .map {
                it.toDomain().copy(
                    page = page,
                    isLastPage = isLastPage
                )
            }
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.LOCAL, t = it)
                Observable.error(exception)
            }
    }

    override fun getCountOfFavoriteMovies(): Observable<Int> {
        val localDataSource = dataSourceFactory.create(type = DataSourceType.LOCAL)
        return localDataSource.getCountOfFavoriteMovies()
    }

    override fun insertFavoriteMovie(
        id: Int,
        title: String,
        overview: String,
        imgUrl: String,
        releaseDate: String
    ): Observable<Unit> {
        val localDataSource = dataSourceFactory.create(type = DataSourceType.LOCAL)
        val favoriteMovie = FavoriteMovieEntity(
            id = id,
            movieTitle = title,
            movieOverview = overview,
            movieImageUrl = imgUrl,
            movieReleaseDate = releaseDate
        )
        return localDataSource.insertFavoriteMovie(favoriteMovie = favoriteMovie)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.LOCAL, t = it)
                Observable.error(exception)
            }
    }

    override fun deleteFavoriteMovie(
        id: Int,
        title: String,
        overview: String,
        imgUrl: String,
        releaseDate: String
    ): Observable<Unit> {
        val localDataSource = dataSourceFactory.create(type = DataSourceType.LOCAL)
        val favoriteMovie = FavoriteMovieEntity(
            id = id,
            movieTitle = title,
            movieOverview = overview,
            movieImageUrl = imgUrl,
            movieReleaseDate = releaseDate
        )
        return localDataSource.deleteFavoriteMovie(favoriteMovie = favoriteMovie)
            .onErrorResumeNext {
                val exception = exceptionFactory.create(type = DataSourceType.LOCAL, t = it)
                Observable.error(exception)
            }
    }

    override fun getFavoriteMovieById(id: Int): Observable<FavoriteMovie> {
        val localDataSource = dataSourceFactory.create(type = DataSourceType.LOCAL)
        return localDataSource.getFavoriteMovieById(id = id)
            .map { it.toDomain() }
    }
}
