package com.dedechandran.data.di

import android.content.Context
import androidx.room.Room
import com.dedechandran.data.datasource.local.AppDatabase
import com.dedechandran.data.datasource.local.dao.FavoriteMovieDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {
    @Provides
    @Singleton
    fun provideRoomDb(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "movie-db"
        ).build()
    }

    @Provides
    @Singleton
    fun provideFavoriteMovieDao(appDatabase: AppDatabase): FavoriteMovieDao {
        return appDatabase.movieDao()
    }
}
