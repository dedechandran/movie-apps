package com.dedechandran.data.di

import com.dedechandran.data.datasource.local.LocalDataSource
import com.dedechandran.data.datasource.local.LocalDataSourceImpl
import com.dedechandran.data.datasource.remote.RemoteDataSource
import com.dedechandran.data.datasource.remote.RemoteDataSourceImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataSourceModule {
    @Binds
    @Singleton
    abstract fun bindRemoteDataSource(remoteDataSourceImpl: RemoteDataSourceImpl): RemoteDataSource

    @Binds
    @Singleton
    abstract fun bindLocalDataSource(localDataSourceImpl: LocalDataSourceImpl): LocalDataSource
}
