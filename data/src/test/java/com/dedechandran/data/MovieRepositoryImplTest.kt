package com.dedechandran.data

import com.dedechandran.data.datasource.DataSource
import com.dedechandran.data.datasource.DataSourceFactory
import com.dedechandran.data.datasource.DataSourceType
import com.dedechandran.data.datasource.local.entity.FavoriteMovieEntity
import com.dedechandran.data.datasource.remote.response.MovieResponse
import com.dedechandran.data.exception.ExceptionFactory
import com.dedechandran.domain.model.FavoriteMovie
import com.dedechandran.domain.model.PopularMovie
import com.dedechandran.domain.model.TopRatedMovie
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Observable
import org.junit.Before
import org.junit.Test

class MovieRepositoryImplTest {

    @MockK
    lateinit var dataSource: DataSource

    @MockK
    lateinit var dataSourceFactory: DataSourceFactory

    @MockK
    lateinit var exceptionFactory: ExceptionFactory

    private lateinit var movieRepositoryImpl: MovieRepositoryImpl

    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        movieRepositoryImpl = MovieRepositoryImpl(
            dataSourceFactory = dataSourceFactory,
            exceptionFactory = exceptionFactory
        )
        every { dataSourceFactory.create(DataSourceType.REMOTE) } returns dataSource
        every { dataSourceFactory.create(DataSourceType.LOCAL) } returns dataSource
    }

    @Test
    fun getPopularMovies_inRepository_shouldProduce_popularMovie() {
        val mockResponse = MovieResponse(
            page = 1,
            totalPage = 1,
            totalResult = 0,
            results = emptyList()
        )
        val actualResult = PopularMovie(
            page = 1,
            isLastPage = true,
            items = mockResponse.results.map {
                PopularMovie.PopularMovieItem(
                    id = it.id,
                    title = it.title.orEmpty(),
                    overview = it.overview.orEmpty(),
                    imgUrl = it.imageUrl.orEmpty(),
                    releaseDate = it.releaseDate.orEmpty()
                )
            }
        )
        every { dataSource.getPopularMovies(1) } returns Observable.just(mockResponse)
        movieRepositoryImpl.getPopularMovies(page = 1)
            .test()
            .assertResult(actualResult)
            .dispose()
        verify { dataSource.getPopularMovies(page = 1) }
    }

    @Test
    fun getTopRatedMovies_inRepository_shouldProduce_topRatedMovie() {
        val mockResponse = MovieResponse(
            page = 1,
            totalPage = 1,
            totalResult = 0,
            results = emptyList()
        )
        val actualResult = TopRatedMovie(
            page = 1,
            isLastPage = true,
            items = mockResponse.results.map {
                TopRatedMovie.TopRatedMovieItem(
                    id = it.id,
                    title = it.title.orEmpty(),
                    overview = it.overview.orEmpty(),
                    imgUrl = it.imageUrl.orEmpty(),
                    releaseDate = it.releaseDate.orEmpty()
                )
            }
        )
        every { dataSource.getTopRatedMovies(1) } returns Observable.just(
            MovieResponse(
                page = 1,
                totalPage = 1,
                totalResult = 0,
                results = emptyList()
            )
        )
        movieRepositoryImpl.getTopRatedMovies(page = 1)
            .test()
            .assertResult(actualResult)
            .dispose()
        verify { dataSource.getTopRatedMovies(page = 1) }
    }

    @Test
    fun getFavoriteMovies_inRepository_shouldProduce_favoriteMovie() {
        val mockResponse = listOf(
            FavoriteMovieEntity(
                id = 1,
                movieTitle = "Title",
                movieOverview = "Overview",
                movieReleaseDate = "2020-12-12",
                movieImageUrl = "imgUrl"
            )
        )
        val actualResult = FavoriteMovie(
            page = 1,
            isLastPage = true,
            items = mockResponse.map {
                FavoriteMovie.FavoriteMovieItem(
                    id = it.id,
                    title = it.movieTitle.orEmpty(),
                    overview = it.movieOverview.orEmpty(),
                    imgUrl = it.movieImageUrl.orEmpty(),
                    releaseDate = it.movieReleaseDate.orEmpty()
                )
            }
        )
        every { dataSource.getFavoriteMovies(any(), any()) } returns Observable.just(mockResponse)
        val mockPage = 1
        val mockLimit = 20
        val mockOffset = (mockPage - 1) * mockLimit
        val mockLastPage = true
        movieRepositoryImpl.getFavoriteMovies(
            page = mockPage,
            limit = mockLimit,
            offset = mockOffset,
            isLastPage = mockLastPage
        )
            .test()
            .assertResult(actualResult)
            .dispose()
        verify { dataSource.getFavoriteMovies(any(), any()) }
    }

    @Test
    fun insertFavoriteMovie_inRepository_shouldSuccess_toInsertFavoriteMovie() {
        val mockId = 1
        val mockTitle = "Title"
        val mockOverview = "MockOverview"
        val mockImgUrl = "ImgUrl"
        val mockReleaseDate = "ReleaseDate"
        val favoriteMovieEntity = FavoriteMovieEntity(
            id = mockId,
            movieTitle = mockTitle,
            movieOverview = mockOverview,
            movieImageUrl = mockImgUrl,
            movieReleaseDate = mockReleaseDate
        )
        every { dataSource.insertFavoriteMovie(favoriteMovieEntity) } returns Observable.just(Unit)
        movieRepositoryImpl.insertFavoriteMovie(
            id = mockId,
            title = mockTitle,
            overview = mockOverview,
            imgUrl = mockImgUrl,
            releaseDate = mockReleaseDate
        )
            .test()
            .assertResult(Unit)
            .assertComplete()
            .dispose()
        verify { dataSource.insertFavoriteMovie(favoriteMovieEntity) }
    }

    @Test
    fun deleteFavoriteMovie_inRepository_shouldSuccess_toDeleteFavoriteMovie() {
        val mockId = 1
        val mockTitle = "Title"
        val mockOverview = "MockOverview"
        val mockImgUrl = "ImgUrl"
        val mockReleaseDate = "ReleaseDate"
        val favoriteMovieEntity = FavoriteMovieEntity(
            id = mockId,
            movieTitle = mockTitle,
            movieOverview = mockOverview,
            movieImageUrl = mockImgUrl,
            movieReleaseDate = mockReleaseDate
        )
        every { dataSource.deleteFavoriteMovie(favoriteMovieEntity) } returns Observable.just(Unit)
        movieRepositoryImpl.deleteFavoriteMovie(
            id = mockId,
            title = mockTitle,
            overview = mockOverview,
            imgUrl = mockImgUrl,
            releaseDate = mockReleaseDate
        )
            .test()
            .assertResult(Unit)
            .assertComplete()
            .dispose()
        verify { dataSource.deleteFavoriteMovie(favoriteMovieEntity) }
    }

    @Test
    fun getFavoriteMovieById_inRepository_shouldProduce_favoriteMovie() {
        val mockResponse = listOf(
            FavoriteMovieEntity(
                id = 1,
                movieTitle = "Title",
                movieOverview = "Overview",
                movieReleaseDate = "2020-12-12",
                movieImageUrl = "imgUrl"
            )
        )
        val actualResult = FavoriteMovie(
            page = 1,
            isLastPage = true,
            items = mockResponse.map {
                FavoriteMovie.FavoriteMovieItem(
                    id = it.id,
                    title = it.movieTitle.orEmpty(),
                    overview = it.movieOverview.orEmpty(),
                    imgUrl = it.movieImageUrl.orEmpty(),
                    releaseDate = it.movieReleaseDate.orEmpty()
                )
            }
        )
        val mockId = 1
        every { dataSource.getFavoriteMovieById(mockId) } returns Observable.just(mockResponse)
        movieRepositoryImpl.getFavoriteMovieById(id = mockId)
            .test()
            .assertResult(actualResult)
            .dispose()
        verify { dataSource.getFavoriteMovieById(id = mockId) }
    }

    @Test
    fun getFavoriteMoviesCount_inRepository_shouldProduce_countOfFavoriteMovies() {
        val actualResult = 10
        every { dataSource.getCountOfFavoriteMovies() } returns Observable.just(actualResult)
        movieRepositoryImpl.getCountOfFavoriteMovies()
            .test()
            .assertResult(actualResult)
            .dispose()
        verify { dataSource.getCountOfFavoriteMovies() }
    }

}
